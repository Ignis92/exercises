// EXERCISE 10.14: Write a lambda that takes two ints and
// returns their sum.

#include "stdafx.h"
#include <iostream>

int main()
{
	int Val1 = 5, Val2 = 3;
	auto f = [](int Int1, int Int2) { return Int1 + Int2; };
	std::cout << "The sum is: " << f(Val1, Val2) << std::endl;

	return 0;
}