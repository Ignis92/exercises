// EXERCISE 9.47: Write a program that finds each numeric
// character and then each alphabetic character in the string
// "ab2c3d7R4E6". Write two versions of the program. The first
// should use find_first_of, and the second find_first_not_of.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Str("ab2c3d7R4E");

	// First version with find_first_of
	/*
	std::string::size_type I = 0;
	std::cout << "Numeric character: ";
	while ((I = Str.find_first_of("0123456789", I)) != std::string::npos)
	{
		std::cout << Str[I];
		++I;
	}

	I = 0;
	std::cout << std::endl << "Alphanumeric character: ";
	while (I < Str.size())
	{
		for (auto J = Str.find_first_of("0123456789", I); I < Str.size() && I != J; ++I)
		{
			std::cout << Str[I];
		}
		++I;
	}
	*/

	// Second version with find_first_not_of
	std::string::size_type I = 0;
	std::cout << "Numeric character: ";
	while (I < Str.size())
	{
		for (auto J = Str.find_first_not_of("0123456789", I); I < Str.size() && I != J; ++I)
		{
			std::cout << Str[I];
		}
		++I;
	}

	I = 0;
	std::cout << std::endl << "Alphanumeric character: ";
	while ((I = Str.find_first_not_of("0123456789", I)) != std::string::npos)
	{
		std::cout << Str[I];
		++I;
	}

	std::cout << std::endl;

	return 0;
}