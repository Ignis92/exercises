// EXERCISE 12.7: Redo the previous exercise, this time
// using shared_ptr.

#include "stdafx.h"
#include <iostream>
#include <memory>
#include <vector>

std::shared_ptr<std::vector<int>> CreateVector()
{
	return std::make_shared<std::vector<int>>();
}

void AssignValues(std::shared_ptr<std::vector<int>> P)
{
	for (size_t I = 0; I != 10; ++I)
	{
		P->push_back(I);
	}
	return;
}

void Print(std::shared_ptr<std::vector<int>> P)
{
	std::cout << "The vector is:" << std::endl;
	for (auto Iter = P->cbegin(); Iter != P->cend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}
	return;
}


int main()
{
	auto P = CreateVector();
	Print(P);
	AssignValues(P);
	Print(P);

	return 0;
}