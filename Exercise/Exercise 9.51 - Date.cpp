#include "stdafx.h"
#include "Date.h"

Date::Date(const std::string & Input)
{
	auto I = Input.find_first_of("0123456789");
	if (I)
	{
		Month = TransformMonthToNumber(Input.substr(0, I - 1));
		std::string::size_type J;
		Day = stoi(Input.substr(I, J = Input.find(',', I)));
		Year = stoi(Input.substr(J + 1));
	}
	else
	{
		Month = stoi(Input.substr(0, I = Input.find('/'))); // assumes english dating system
		std::string::size_type J;
		++I;
		Day = stoi(Input.substr(I, J = Input.find('/',I)));
		++I;
		++J;
		Year = stoi(Input.substr(J, Input.find('/',J)));
	}
}

unsigned Date::TransformMonthToNumber(const std::string & Input)
{
	if (Input == "January" || Input == "Jan")
	{
		return 1;
	}
	else
	{
		if (Input == "February" || Input == "Feb")
		{
			return 2;
		}
		else
		{
			if (Input == "March" || Input == "Mar")
			{
				return 3;
			}
			else
			{
				if (Input == "April" || Input == "Apr")
				{
					return 4;
				}
				else
				{
					if (Input == "May")
					{
						return 5;
					}
					else
					{
						if (Input == "June" || Input == "Jun")
						{
							return 6;
						}
						else
						{
							if (Input == "July" || Input == "Jul")
							{
								return 7;
							}
							else
							{
								if (Input == "August" || Input == "Aug")
								{
									return 8;
								}
								else
								{
									if (Input == "September" || Input == "Sep")
									{
										return 9;
									}
									else
									{
										if (Input == "October" || Input == "Oct")
										{
											return 10;
										}
										else
										{
											if (Input == "November" || Input == "Nov")
											{
												return 11;
											}
											else
											{
												if (Input == "December" || Input == "Dec")
												{
													return 12;
												}
												else
												{
													return 0;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
