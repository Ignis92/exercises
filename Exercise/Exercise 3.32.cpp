// EXERCISE 3.32: Copy the array you defined in the previous
// exercise into another array. Rewrite your program to use
// vectors.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	// Array version
	/*
	constexpr int size = 10;
	int Arr[size];
	int Count = 0;

	for (auto &i:Arr)
	{
	i = Count;
	++Count;
	}

	int ArrCopy[size];
	for (int i = 0; i < size; ++i)
	{
	ArrCopy[i] = Arr[i];
	}

	std::cout << "The copied array's elements are:" << std::endl;
	for (auto i : ArrCopy)
	{
	std::cout << i << std::endl;
	}
	*/

	// Vector version
	std::vector<int> Vect(10);

	for (decltype(Vect.size()) i = 0; i < Vect.size(); ++i)
	{
		Vect[i] = i;
	}

	std::vector<int> VectCopy = Vect;

	std::cout << "The copied vector's elements are:" << std::endl;
	for (auto i : VectCopy)
	{
		std::cout << i << std::endl;
	}
	return 0;
}