// EXERCISE 10.22: Rewrite the program to count words
// of size 6 or less using functions in place of the lambdas.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>

void ElimDups(std::vector<std::string> &Target)
{
	sort(Target.begin(), Target.end());
	auto Tail = std::unique(Target.begin(), Target.end());
	Target.erase(Tail, Target.end());
}

bool Compare(const std::string &Str1, const std::string &Str2)
{
	return Str1.size() < Str2.size();
}

bool IsBigEnough(const std::string &Str, std::string::size_type Size)
{
	return Str.size() > Size;
}

void Biggies(std::vector<std::string> &Target, std::string::size_type Size)
{
	ElimDups(Target);
	auto Check = std::bind(IsBigEnough, std::placeholders::_1, Size);

	std::stable_sort(Target.begin(), Target.end(), Compare);

	std::cout << "The " << std::count_if(Target.cbegin(), Target.cend(), Check);
	std::cout << " word(s) big enough are (is): " << std::endl;
	for (auto Iter = Target.begin(), IterEnd = std::stable_partition(Target.begin(), Target.end(), Check);
		Iter != IterEnd; ++Iter)
	{
		std::cout << *Iter << std::endl;
	}

}

int main()
{
	std::vector<std::string> words{ "the","quick","red","fox","jumps","over","the","slow","red","turtle" };
	std::string::size_type Size = 3;

	Biggies(words, Size);

	return 0;
}