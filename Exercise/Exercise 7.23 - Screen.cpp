#include "stdafx.h"
#include "Screen.h"

Screen & Screen::Move(Pos Row, Pos Column)
{
	CursorPosition = Row * Width + Column;
	return *this;
}
