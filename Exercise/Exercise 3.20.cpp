// EXERCISE 3.20: Read a set of integers into a vector.
// Print the sum of each pair of adjacent elements. Change
// your program so that it prints the sum of the first and
// last elements, followed by the sum of the second and
// second - to - last, and so on.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<int> Vect;
	int Elem;

	std::cout << "Input an integer:" << std::endl;
	while (std::cin >> Elem)
	{
		Vect.push_back(Elem);
		std::cout << "Input an integer or ctrl-z to terminate:" << std::endl;
	}


	// First part of the exercise
	/*
	for (decltype(Vect.size()) i = 0; i < Vect.size() - 1; ++i)
	{
	std::cout << "Sum of the element number " << i + 1 <S< " and the next one: ";
	std::cout << Vect[i] + Vect[i + 1] << std::endl;
	}
	*/

	// Second part of the exercise
	for (decltype(Vect.size()) i = 0; i < Vect.size() / 2; ++i)
	{
		std::cout << "Sum of the element number " << i + 1;
		std::cout << " and the corresponding element number " << Vect.size() - i << ": ";
		std::cout << Vect[i] + Vect[Vect.size() - i - 1] << std::endl;
	}
	std::cout << std::endl;

	return 0;
}