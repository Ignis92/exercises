// EXERCISE 5.21: Revise the program from the exercise in �
// 5.5.1 (p. 191) so that it looks only for duplicated words that
// start with an uppercase letter.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	bool Repeated = false;
	std::string CurrStr, PrevStr;

	std::cout << "Input a string:" << std::endl;
	while (std::cin >> CurrStr)
	{
		if (CurrStr == PrevStr && isupper(CurrStr[0]))
		{
			Repeated = true;
			break;
		}
		else
		{
			PrevStr = CurrStr;
		}
	}

	if (Repeated == true)
	{
		std::cout << "The word " << CurrStr << " occurs twice in succession." << std::endl;
	}
	else
	{
		std::cout << "There was no repetition." << std::endl;
	}
	return 0;
}