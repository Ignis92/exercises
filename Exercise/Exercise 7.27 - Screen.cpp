#include "stdafx.h"
#include "Screen.h"

Screen & Screen::Move(Pos Row, Pos Column)
{
	CursorPosition = Row * Width + Column;
	return *this;
}

Screen & Screen::Set(char ToChange)
{
	Contents[CursorPosition] = ToChange;
	return *this;
}

Screen & Screen::Set(Pos Row, Pos Column, char ToChange)
{
	Contents[Row * Width + Column] = ToChange;
	return *this;
}

Screen & Screen::Display(std::ostream &Output)
{
	DoDisplay(Output);
	return *this;
}

const Screen & Screen::Display(std::ostream &Output) const
{
	DoDisplay(Output);
	return *this;
}

void Screen::DoDisplay(std::ostream &Output) const
{
	Output << Contents;
	return;
}
