// EXERCISE 10.3: Use accumulate to sum the elements in a
// vector<int>.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <numeric>

int main()
{
	std::vector<int> VectInt{ 1,2,3,4,5,6,7,8,9 };

	std::cout << "Total sum in the vector is " << std::accumulate(VectInt.cbegin(), VectInt.cend(), 0) << std::endl;

	return 0;
}