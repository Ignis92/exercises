// EXERCISE 7.5: Provide operations in your Person class to
// return the name and address. Should these functions be
// const? Explain your choice.

#pragma once

#include <string>

struct Person
{
	std::string GetName() const { return Name; }
	std::string GetAddress() const { return Address; }

	std::string Name;
	std::string Address;
};