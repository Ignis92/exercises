// EXERCISE 7.40: Choose one of the following
// abstractions (or an abstraction of your own choosing).
// Determine what data are needed in the class. Provide
// an appropriate set of constructors. Explain your
// decisions.
// (a)Book
// (b) Date
// (c) Employee
// (d) Vehicle
// (e) Object
// (f) Tree

#pragma once

#include <string>

class Book
{
public:
	Book() = default;
	Book(std::string T) : Title(T) {}
	Book(std::string T, unsigned N) : Title(T), NumberOfPages(N) {}
	Book(std::string T, double C) : Title(T), Cost(C) {}
	Book(std::string T, bool E) : Title(T), isEconomicVersion(E) {}
	Book(std::string T, unsigned N, double C) : Title(T), NumberOfPages(N), Cost(C) {}
	Book(std::string T, unsigned N, bool E) : Title(T), NumberOfPages(N), isEconomicVersion(E) {}
	Book(std::string T, double C, bool E) : Title(T), Cost(C), isEconomicVersion(E) {}
	Book(std::string T, unsigned N, double C, bool E) : Title(T), NumberOfPages(N), Cost(C), isEconomicVersion(E) {}

private:
	std::string Title;
	unsigned NumberOfPages = 0;
	double Cost = 0;
	bool isEconomicVersion = false;
};