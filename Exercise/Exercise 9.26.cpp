// EXERCISE 9.26: Using the following definition of ia,
// copy ia into a vector and into a list. Use the single-
// iterator form of erase to remove the elements with odd
// values from your list and the even values from your
// vector.
// int ia[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 55, 89 };

#include "stdafx.h"
#include <iostream>
#include <list>
#include <vector>

int main()
{
	int ia[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 55, 89 };
	std::list<int> ListInt(std::begin(ia), std::end(ia));
	std::vector<int> VectInt(std::begin(ia), std::end(ia));

	for (auto Iter = ListInt.cbegin(); Iter != ListInt.cend();)
	{
		if (*Iter % 2) // If odd (surprisingly, it's the opposite of what it looks like)
		{
			Iter = ListInt.erase(Iter);
		}
		else
		{
			++Iter;
		}
	}

	for (auto Iter = VectInt.cbegin(); Iter != VectInt.cend();)
	{
		if (*Iter % 2) // If odd
		{
			++Iter;
		}
		else
		{
			Iter = VectInt.erase(Iter);
		}
	}

	std::cout << "Printing the list:" << std::endl;
	for (auto Elem : ListInt)
	{
		std::cout << Elem << std::endl;
	}

	std::cout << std::endl;

	std::cout << "Printing the vector:" << std::endl;
	for (auto Elem : VectInt)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}