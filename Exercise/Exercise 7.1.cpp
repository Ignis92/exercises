// EXERCISE 7.1: Write a version of the transaction-
// processing program from � 1.6 (p. 24) using the
// Sales_data class you defined for the exercises in � 2.6.1
// (p. 72).

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>


int main()
{
	SalesData Total;

	if (Read(std::cin, std::cout, Total))
	{
		SalesData Tmp;
		while (Read(std::cin, std::cout, Tmp))
		{
			if (Total.BookNumber == Tmp.BookNumber)
			{
				Add(Total, Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBN differs!" << std::endl;
		return -1;
	}
}