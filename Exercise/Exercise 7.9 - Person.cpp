#include "stdafx.h"
#include "Person.h"

std::istream & Person::Read(std::istream & Input, std::ostream & Output)
{
	Output << "Input the name: ";
	if (getline(Input, Name))
	{
		Output << "Input the address: ";
		if (!getline(Input, Address))
		{
			Output << "Incorrect input address." << std::endl;
		}
	}
	else
	{
		Output << "Incorrect input name." << std::endl;
	}

	return Input;
}

std::ostream & Person::Print(std::ostream & Output)
{
	Output << "Name: " << Name << std::endl;
	Output << "Address: " << Address << std::endl;

	return Output;
}
