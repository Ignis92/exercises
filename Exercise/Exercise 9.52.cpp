// EXERCISE 9.52: Use a stack to process parenthesized
// expressions. When you see an open parenthesis, note
// that it was seen. When you see a close parenthesis
// after an open parenthesis, pop elements down to and
// including the open parenthesis off the stack. push a
// value onto the stack to indicate that a parenthesized
// expression was replaced.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <stack>

int main()
{
	std::stack<char> ProcessedExpression;
	std::string Line, Expression;

	std::cout << "Insert expression:" << std::endl;
	bool IsProcessing = false;
	while (getline(std::cin, Line))
	{
		for (const auto C : Line)
		{
			if (IsProcessing == false)
			{
				if (C == '(')
				{
					std::cout << "New parenthesis opened" << std::endl;
					IsProcessing = true;
					ProcessedExpression.push(C);
				}
			}
			else
			{
				ProcessedExpression.push(C);
				if (C == ')')
				{
					IsProcessing = false;
					Expression.clear();
					auto Beg = Expression.cbegin();
					for (char H = ProcessedExpression.top(); !ProcessedExpression.empty();)
					{
						H = ProcessedExpression.top();
						Beg = Expression.insert(Beg, H);
						ProcessedExpression.pop();
					}
					std::cout << Expression << std::endl;
				}
			}
		}
	}

	return 0;
}