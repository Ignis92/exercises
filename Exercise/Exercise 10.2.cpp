// EXERCISE 10.2: Repeat the previous program, but read
// values into a list of strings.

#include "stdafx.h"
#include <iostream>
#include <list>
#include <string>
#include <algorithm>

int main()
{
	std::list<std::string> ListStr{ "abc","def","ghi","lmn","abc","opq" };

	std::cout << "\"abc\" appears " << std::count(ListStr.cbegin(), ListStr.cend(), "abc") << " times." << std::endl;

	return 0;
}