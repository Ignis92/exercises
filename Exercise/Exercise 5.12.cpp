// EXERCISE 5.12: Modify our vowel - counting program so that
// it counts the number of occurrences of the following two - character
// sequences: ff, fl, and fi.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Line;

	std::cout << "Input a line: ";
	while (getline(std::cin, Line))
	{
		int fCnt = 0;

		for (auto it = Line.cbegin(); it != Line.end(); ++it)
		{
			switch (*it)
			{
			case 'f':
				if (*(it + 1) == 'f' || *(it + 1) == 'i' || *(it + 1) == 'l')
				{
					++fCnt;
				}
				++it;
				break;
			default:
				;
				break;
			}
		}
		std::cout << "The number of ff, fl or fi sequences in the inserted line is: " << fCnt << std::endl;
		std::cout << "Input a line or ctrl-z to terminate: ";
	}

	return 0;
}