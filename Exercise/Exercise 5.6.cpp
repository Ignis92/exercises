// EXERCISE 5.6: Rewrite your grading program to use the
// conditional operator (� 4.7, p. 151) in place of the
// if�else statement.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	unsigned int Score;
	char *IntGrade[10] = { "1","2","3","4","5","6","7","8","9","10" };
	std::string FinalGrade;

	std::cout << "Input a score: ";
	std::cin >> Score;
	int Rest = Score % 10;

	FinalGrade = (Rest < 9) ? IntGrade[Score / 10 - 1] : IntGrade[Score / 10];
	FinalGrade = (Score < 60) ? "Insufficient" :
		(Rest == 3 || Rest == 4) ? FinalGrade + "+" :
		(Rest == 5 || Rest == 6) ? FinalGrade + " and half" :
		(Rest == 7 || Rest == 8) ? FinalGrade + "/" + IntGrade[Score / 10] :
		(Rest == 9) ? FinalGrade + "-" : FinalGrade;

	std::cout << FinalGrade << std::endl;

	return 0;
}