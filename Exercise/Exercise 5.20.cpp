// EXERCISE 5.20: Write a program to read a sequence of
// strings from the standard input until either the same
// word occurs twice in succession or all the words have
// been read. Use a while loop to read the text one word at
// a time. Use the break statement to terminate the loop if
// a word occurs twice in succession. Print the word if it
// occurs twice in succession, or else print a message
// saying that no word was repeated.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	bool Repeated = false;
	std::string CurrStr, PrevStr;

	std::cout << "Input a string:" << std::endl;
	while (std::cin >> CurrStr)
	{
		if (CurrStr == PrevStr)
		{
			Repeated = true;
			break;
		}
		else
		{
			PrevStr = CurrStr;
		}
	}

	if (Repeated == true)
	{
		std::cout << "The word " << CurrStr << " occurs twice in succession." << std::endl;
	}
	else
	{
		std::cout << "There was no repetition." << std::endl;
	}
	return 0;
}