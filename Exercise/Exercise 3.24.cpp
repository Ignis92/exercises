// EXERCISE 3.24: Redo the last exercise from � 3.3.3 (p.
// 105) using iterators.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> Vect;
	int Elem;

	std::cout << "Input an integer:" << std::endl;
	while (std::cin >> Elem)
	{
		Vect.push_back(Elem);
		std::cout << "Input an integer or ctrl-z to terminate:" << std::endl;
	}


	// First part of the exercise
	/*
	for (auto it = Vect.cbegin(); it != Vect.cend() - 1; ++it)
	{
	std::cout << "Sum of the element number " << it - Vect.cbegin() + 1 << " and the next one: ";
	std::cout << *it + *(it + 1) << std::endl;
	}
	*/

	// Second part of the exercise
	for (auto it = Vect.cbegin(); it != Vect.cbegin() + (Vect.cend() - Vect.cbegin()) / 2; ++it)
	{
		std::cout << "Sum of the element number " << it - Vect.cbegin() + 1;
		std::cout << " and the correponding element number " << Vect.cend() - it << ": ";
		std::cout << *it + *(Vect.cbegin() + (Vect.cend() - it - 1)) << std::endl;
	}

	std::cout << std::endl;
	return 0;
}