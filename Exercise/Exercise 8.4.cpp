// EXERCISE 8.4: Write a function to open a file for input and
// read its contents into a vector of strings, storing each line
// as a separate element in the vector.


#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &InputFile)
{
	std::vector<std::string> Result;
	std::ifstream File(InputFile);

	std::string Line;
	while (getline(File, Line))
	{
		Result.push_back(Line);
	}
	
	return Result;
}

int main()
{
	std::string File = "C:\\Users\\abocco\\Desktop\\Lavori\\dizionarizzazione\\file u7p40\\me_10_ecap_hft_core.sas";
	std::vector<std::string> Text;
	Text=ReadFile(File);

	for (auto l : Text)
	{
		std::cout << l << std::endl;
	}

	return 0;
}