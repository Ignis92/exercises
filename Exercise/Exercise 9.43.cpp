// EXERCISE 9.43: Write a function that takes three
// strings, s, oldVal, and newVal. Using iterators, and the
// insert and erase functions replace all instances of oldVal
// that appear in s by newVal. Test your function by using it
// to replace common abbreviations, such as “tho” by
// “though” and “thru” by “through”.

#include "stdafx.h"
#include <iostream>
#include <string>

std::string &Substitute(std::string &s, const std::string &oldVal, const std::string &newVal)
{
	for (auto OutIter = s.cbegin(); OutIter != s.cend(); ++OutIter)
	{
		auto InIter = oldVal.cbegin(), WordStart = OutIter;
		while (InIter != oldVal.cend() && OutIter != s.cend() && *OutIter == *InIter)
		{
			++OutIter;
			++InIter;
		}

		if (InIter == oldVal.cend())
		{
			WordStart = s.erase(WordStart, OutIter);
			WordStart = s.insert(WordStart, newVal.cbegin(), newVal.cend());
			OutIter = WordStart + newVal.size();
		}
	}

	return s;
}

int main()
{
	std::string s("Tho this, we got there. It is good, Tho could be better"), oldVal("Tho"), newVal("Though");

	std::cout << "String after correction is:" << std::endl;
	std::cout << Substitute(s, oldVal, newVal) << std::endl;

	return 0;
}