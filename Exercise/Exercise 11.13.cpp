// EXERCISE 11.13: There are at least three ways to
// create the pairs in the program for the previous
// exercise. Write three versions of that program, creating
// the pairs in each way. Explain which form you think is
// easiest to write and understand, and why.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <utility>
#include <string>

int main()
{
	std::string Str;
	int Int;
	std::vector<std::pair<std::string, int>> VectPair;

	std::cout << "Keep inserting a string first and an integer second or ctrl-z to terminate:" << std::endl;
	while (std::cin >> Str && std::cin >> Int)
	{
		// 1): std::pair<std::string, int> P(Str, Int)		- outside and pass P to push_back
		// 2): std::pair<std::string, int> P = { Str, Int } - outside and pass P to push_back
		// 3):
		VectPair.push_back(std::make_pair(Str, Int));
	}

	std::cout << "The vector is:" << std::endl;
	for (const auto &Elem : VectPair)
	{
		std::cout << "the word is: " << Elem.first << "; the int is: " << Elem.second << "." << std::endl;
	}

	return 0;
}