// EXERCISE 4.22: Extend the program that assigned high
// pass, pass, and fail grades to also assign low pass for
// grades between 60 and 75 inclusive. Write two
// versions: One version that uses only conditional
// operators; the other should use one or more if
// statements.Which version do you think is easier to
// understand and why ?

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<std::string> Finalgrades;
	unsigned Grade;

	std::cout << "Input a grade:" << std::endl;
	while (std::cin >> Grade)
	{
		Finalgrades.push_back((Grade > 90) ? "high pass" : (Grade > 75) ? "pass" : (Grade > 60) ? "low pass" : "fail");
		std::cout << "Input a grade or ctrl-z to terminate:" << std::endl;
	}

	std::cout << "Final grades:" << std::endl;
	for (auto i : Finalgrades)
	{
		std::cout << i << std::endl;
	}

	return 0;
}