// EXERCISE 7.15: Add appropriate constructors to your
// Person class.

#pragma once

#include <string>

struct Person
{
	Person() {}
	Person(std::string N) : Name(N) {}
	Person(std::string N, std::string A) : Name(N), Address(A) {}
	Person(std::istream &Input, std::ostream &Output) { Read(Input, Output); }
		
	std::string GetName() const { return Name; }
	std::string GetAddress() const { return Address; }

	std::istream &Read(std::istream &Input, std::ostream &Output);
	std::ostream &Print(std::ostream &Output);

	std::string Name;
	std::string Address;
};