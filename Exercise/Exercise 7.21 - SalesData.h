// EXERCISE 7.21: Update your Sales_data class to hide its
// implementation. The programs you�ve written to use
// Sales_data operations should still continue to work.
// Recompile those programs with your new class
// definition to verify that they still work.

#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	friend SalesData Add(const SalesData &Data1, const SalesData &Data2);
	friend std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
	friend std::ostream &Print(std::ostream &Output, const SalesData &Target);
public:
	SalesData() {}
	SalesData(const std::string S) : BookNumber(S) {}
	SalesData(const std::string S, unsigned U, double R) : BookNumber(S), UnitSold(U), Revenue(R) {}
	SalesData(std::istream & Input, std::ostream & Output);

	std::string ISBN() const { return BookNumber; }
	SalesData &Combine(const SalesData &ToAdd);

private:
	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

SalesData Add(const SalesData &Data1, const SalesData &Data2);
std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
std::ostream &Print(std::ostream &Output, const SalesData &Target);