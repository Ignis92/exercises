// EXERCISE 11.12: Write a program to read a sequence of
// strings and ints, storing each into a pair. Store the
// pairs in a vector.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <utility>
#include <string>

int main()
{
	std::string Str;
	int Int;
	std::vector<std::pair<std::string, int>> VectPair;

	std::cout << "Keep inserting a string first and an integer second or ctrl-z to terminate:" << std::endl;
	while (std::cin >> Str && std::cin >> Int)
	{
		VectPair.emplace_back(Str, Int);
	}

	std::cout << "The vector is:" << std::endl;
	for (const auto &Elem : VectPair)
	{
		std::cout << "the word is: " << Elem.first << "; the int is: " << Elem.second << "." << std::endl;
	}

	return 0;
}