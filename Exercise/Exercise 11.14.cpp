// EXERCISE 11.14: Extend the map of children to their
// family name that you wrote for the exercises in §
// 11.2.1 (p. 424) by having the vector store a pair that
// holds a child’s name and birthday.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

void AddChild(std::map<std::string, std::vector<std::pair<std::string, std::string>>> &Target,
	const std::string &Family, const std::pair<std::string, std::string> &NewChild)
{
	auto Iter = Target.find(Family);
	if (Iter == Target.end())
	{
		std::cout << "Family not found" << std::endl;
	}
	else
	{
		Iter->second.push_back(NewChild);
	}
}

void AddFamily(std::map<std::string, std::vector<std::pair<std::string, std::string>>> &Target,
	const std::string &NewFamily,
	const std::vector<std::pair<std::string, std::string>> &Children)
{
	auto Iter = Target.find(NewFamily);
	if (Iter == Target.end())
	{
		Target.insert({ NewFamily,Children });
		return;
	}
	else
	{
		std::cout << "Family already in the map" << std::endl;
	}
}


int main()
{
	std::map<std::string, std::vector<std::pair<std::string, std::string>>> FamilyChildrenMap;
	std::string Family;
	std::pair<std::string, std::string> Child;
	std::vector<std::pair<std::string, std::string>> Children;

	std::cout << "FIRST SECTION: adding a whole family" << std::endl;
	std::cout << "Insert the name of the family or ctrl-z if you want to skip" << std::endl;
	while (std::cin >> Family)
	{
		Children.clear();
		std::cout << "Insert the name and birth date of a child or ctrl-z if you want to skip" << std::endl;
		while (std::cin >> Child.first && std::cin >> Child.second)
		{
			Children.push_back(Child);
			std::cout << "Insert the name of an additional child or ctrl-z if you want to skip" << std::endl;
		}
		AddFamily(FamilyChildrenMap, Family, Children);

		std::cin.clear();
		std::cout << "Insert the name of a new family or ctrl-z if you want to skip" << std::endl;
	}

	std::cin.clear();
	std::cout << "SECOND SECTION: adding a child to a family" << std::endl;
	std::cout << "Insert the name of the family and then the name and birthdate of the child or ctrl-z if you want to skip" << std::endl;
	while (std::cin >> Family && std::cin >> Child.first && std::cin >> Child.second)
	{
		AddChild(FamilyChildrenMap, Family, Child);
		std::cout << "Insert the name of a new family and then the name and birthdate of the child or ctrl-z if you want to skip" << std::endl;
	}


	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : FamilyChildrenMap)
	{
		std::cout << "The family " << Elem.first << " has the following children with respective birthdates:" << std::endl;
		for (const auto &ChildElem : Elem.second)
		{
			std::cout << ChildElem.first << ", born " << ChildElem.second << std::endl;
		}
	}

	return 0;
}