// EXERCISE 7.3: Revise your transaction - processing
// program from � 7.1.1 (p. 256) to use these members.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>


int main()
{
	SalesData Total;

	if (Read(std::cin, std::cout, Total))
	{
		SalesData Tmp;
		while (Read(std::cin, std::cout, Tmp))
		{
			if (Total.ISBN() == Tmp.ISBN())
			{
				Total.Combine(Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBN differs!" << std::endl;
		return -1;
	}
}