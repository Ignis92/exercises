// EXERCISE 10.1: The algorithm header defines a function
// named count that, like find, takes a pair of iterators and
// a value. count returns a count of how often that value
// appears. Read a sequence of ints into a vector and print
// the count of how many elements have a given value.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
	std::vector<int> VectInt{ 1,0,1,3,0,73,1,6,0,24,6 };

	std::cout << "0 appears " << std::count(VectInt.cbegin(), VectInt.cend(), 0) << " times." << std::endl;

	return 0;
}