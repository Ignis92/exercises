// EXERCISE 6.12: Rewrite the program from exercise 6.10
// in � 6.2.1 (p. 210) to use references instead of pointers
// to swap the value of two ints. Which version do you
// think would be easier to use and why?

#include "stdafx.h"
#include <iostream>

void Swap(int &First, int &Second)
{
	int Tmp = Second;
	Second = First;
	First = Tmp;

	return;
}

int main()
{
	int Int1 = 5, Int2 = 10;

	std::cout << "Before swapping:" << std::endl;
	std::cout << "First integer: " << Int1 << ". Second Integer: " << Int2 << "." << std::endl;

	Swap(Int1, Int2);

	std::cout << "After swapping:" << std::endl;
	std::cout << "First integer: " << Int1 << ". Second Integer: " << Int2 << "." << std::endl;

	return 0;
}