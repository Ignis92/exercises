// EXERCISE 8.9: Use the function you wrote for the first
// exercise in § 8.1.2 (p. 314) to print the contents of an
// istringstream object.

#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

std::istream &Read(std::istream &Input)
{
	std::string Elem;
	while (Input >> Elem)
	{
		std::cout << Elem << std::endl;
	}


	std::cin.clear();
	return Input;
}

int main()
{
	std::string Str("Hello world");
	std::istringstream IStr(Str);

	Read(IStr);

	return 0;
}