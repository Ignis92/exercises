// EXERCISE 9.20: Write a program to copy elements from
// a list<int> into two deques. The even - valued elements
// should go into one deque and the odd ones into the
// other.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <list>
#include <deque>

int main()
{
	std::list<int> Result{ 1,2,3,4,5,6,7,8 };
	std::deque<int> Even, Odd;
	
	{
		bool isEven = false; // First element is 1
			for (auto Iter = Result.cbegin(); Iter != Result.cend(); ++Iter)
			{
				if (isEven)
				{
					Even.push_back(*Iter);
				}
				else
				{
					Odd.push_back(*Iter);
				}
				isEven = !isEven;
			}
	}

	std::cout << "Even vector" << std::endl;
	for (const auto Elem:Even)
	{
		std::cout << Elem << std::endl;
	}

	std::cout << "Odd vector" << std::endl;
	for (const auto Elem : Odd)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}