// EXERCISE 4.21: Write a program to use a conditional
// operator to find the elements in a vector<int> that have
// odd value and double the value of each such element.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> Vect{ 1,5,2,4,3,6,9 };

	std::cout << "The vector is:" << std::endl;

	for (auto &i : Vect)
	{
		std::cout << i << std::endl;
		i = ((i % 2) ? i * 2 : i);
	}

	std::cout << "The new vector is:" << std::endl;
	for (auto i : Vect)
	{
		std::cout << i << std::endl;
	}

	return 0;
}