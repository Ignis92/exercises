// EXERCISE 10.29: Write a program using stream iterators to
// read a text file into a vector of strings.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <string>
#include <fstream>

int main()
{
	std::ifstream File("Text.TXT");
	std::istream_iterator<std::string> InputIter(File), EndOfFile;
	std::vector<std::string> Text;

	copy(InputIter, EndOfFile, back_inserter(Text));

	for (const auto &Elem : Text)
	{
		std::cout << Elem << std::endl;
	}
	return 0;
}