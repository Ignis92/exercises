// EXERCISE 6.21: Write a function that takes an int and a
// pointer to an int and returns the larger of the int value or
// the value to which the pointer points. What type should you
// use for the pointer?

#include "stdafx.h"
#include <iostream>

int LargerInt(const int Int, const int *PInt)
{
	if (Int > *PInt)
	{
		return Int;
	}
	else
	{
		return *PInt;
	}
}

int main()
{
	int TestInt = 7, PointedInt = 3, *TestPointer = &PointedInt;

	std::cout << "Integer: " << TestInt << ". Pointed Integer: " << *TestPointer << "." << std::endl;

	std::cout << "Bigger: " << LargerInt(TestInt, TestPointer) << std::endl;

	return 0;
}