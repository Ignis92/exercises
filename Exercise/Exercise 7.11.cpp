// EXERCISE 7.11: Add constructors to your Sales_data
// class and write a program to use each of the
// constructors.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>


int main()
{

	Print(std::cout, SalesData());
	Print(std::cout, SalesData("IT10"));
	Print(std::cout, SalesData("IT10", 10, 53.70));
	Print(std::cout, SalesData(std::cin, std::cout));

}