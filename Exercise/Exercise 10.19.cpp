// EXERCISE 10.19: Rewrite the previous exercise to use
// stable_partition, which like stable_sort maintains the
// original element order in the paritioned sequence.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void ElimDups(std::vector<std::string> &Target)
{
	sort(Target.begin(), Target.end());
	auto Tail = std::unique(Target.begin(), Target.end());
	Target.erase(Tail, Target.end());
}

void Biggies(std::vector<std::string> &Target, std::string::size_type Size)
{
	ElimDups(Target);

	std::stable_sort(Target.begin(), Target.end(),
		[](const std::string &Str1, const std::string &Str2) { return Str1.size() < Str2.size(); });

	std::cout << "The words big enough are: " << std::endl;
	for (auto Iter = std::stable_partition(Target.begin(), Target.end(),
		[Size](const std::string &Str) { return Str.size() <= Size; });
		Iter != Target.cend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}

}

int main()
{
	std::vector<std::string> words{ "the","quick","red","fox","jumps","over","the","slow","red","turtle" };
	std::string::size_type Size = 3;

	Biggies(words, Size);

	return 0;
}