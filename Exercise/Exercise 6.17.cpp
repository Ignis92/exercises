// EXERCISE 6.17: Write a function to determine whether a
// string contains any capital letters. Write a function to
// change a string to all lowercase. Do the parameters you
// used in these functions have the same type? If so, why? If
// not, why not?

#include "stdafx.h"
#include <iostream>
#include <string>

bool HasCapital(const std::string &String)
{
	for (auto &c : String)
	{
		if (isupper(c))
			return true;
	}

	return false;
}

void Capitalize(std::string &String)
{
	for (auto &c : String)
	{
		c = toupper(c);
	}

	return;
}

int main()
{
	std::string TestString = "BellA";

	std::cout << "Starting string:" << std::endl;
	std::cout << TestString << std::endl << std::endl;

	if (HasCapital(TestString))
	{
		std::cout << "The string has uppercase letter(s)." << std::endl;
	}
	else
	{
		std::cout << "The string has no uppercase letter." << std::endl;
	}
	std::cout << std::endl;

	Capitalize(TestString);

	std::cout << "After capitalization:" << std::endl;
	std::cout << TestString << std::endl;

	return 0;
}