// EXERCISE 10.9: Implement your own version of
// elimDups. Test your program by printing the vector after
// you read the input, after the call to unique, and after the
// call to erase.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void ElimDups(std::vector<std::string> &Target)
{
	sort(Target.begin(), Target.end());
	auto Tail = std::unique(Target.begin(), Target.end());
	Target.erase(Tail,Target.end());
}

int main()
{
	std::vector<std::string> VectStr{ "abc","def","abc","ghi","lmn","def","opq","abc" };

	ElimDups(VectStr);

	std::cout << "The vector is:" << std::endl;
	for (const auto Elem : VectStr)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}