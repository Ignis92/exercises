// EXERCISE 11.3: Write your own version of the wordcounting
// program.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>

int main()
{
	std::map<std::string,unsigned> WordCount;
	std::string Word;

	while (std::cin >> Word)
	{
		++WordCount[Word];
	}

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : WordCount)
	{
		std::cout << "The word \"" << Elem.first << "\" appears " << Elem.second << " time(s)." << std::endl;
	}

	return 0;
}