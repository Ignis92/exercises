// EXERCISE 9.49: A letter has an ascender if, as with d or f,
// part of the letter extends above the middle of the line. A
// letter has a descender if, as with p or g, part of the letter
// extends below the line. Write a program that reads a file
// containing words and reports the longest word that contains
// neither ascenders nor descenders.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>

int main()
{
	std::string CurrWord, MaxWord;
	std::ifstream File("Text.TXT");

	while (File >> CurrWord)
	{
		if (CurrWord.size() > MaxWord.size() && CurrWord.find_first_of("bdfghjklpqty") == std::string::npos)
		{
			MaxWord = CurrWord;
		}
	}

	if (MaxWord == "")
	{
		std::cout << "No word found that is not an ascender or descender." << std::endl;
	}
	else
	{
		std::cout << "The biggest word found that is not an ascender or descender is: ";
		std::cout << MaxWord << "." << std::endl;
	}

	std::cout << std::endl;

	return 0;
}