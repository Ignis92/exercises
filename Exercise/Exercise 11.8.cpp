// EXERCISE 11.8: Write a program that stores the
// excluded words in a vector instead of in a set. What are
// the advantages to using a set?

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main()
{
	std::map<std::string, std::size_t> WordCount;
	std::vector<std::string> Exclude = { "The", "But", "And", "Or", "An", "A", "the", "but", "and", "or", "an", "a" };
	std::string Word;

	std::cout << "Keep inserting strings or ctrl-z to terminate:" << std::endl;
	while (std::cin >> Word)
	{
		if (std::find(Exclude.cbegin(),Exclude.cend(),Word)==Exclude.cend())
		{
			++WordCount[Word];
		}
	}

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : WordCount)
	{
		std::cout << "The word \"" << Elem.first << "\" appears " << Elem.second << " time(s)." << std::endl;
	}
	return 0;
}