// EXERCISE 10.34: Use reverse_iterators to print a vector in
// reverse order.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> VectInt{ 0,1,2,3,4,5,6,7,8,9 };

	std::cout << "Printing the vector in reverse order:" << std::endl;
	for (auto Iter = VectInt.crbegin(); Iter != VectInt.crend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}

	return 0;
}