// EXERCISE 11.23: Rewrite the map that stored vectors of
// children’s names with a key that is the family last name
// for the exercises in § 11.2.1 (p. 424) to use a multimap.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>

void AddChild(std::multimap<std::string, std::string> &Target, const std::string &Family, const std::string &NewChild)
{
	Target.insert({ Family,NewChild });
}

void AddFamily(std::map<std::string, std::vector<std::string>> &Target, const std::string &NewFamily,
	const std::vector<std::string> &Children)
{
	auto Iter = Target.find(NewFamily);
	if (Iter == Target.end())
	{
		Target.insert({ NewFamily,Children });
		return;
	}
	else
	{
		std::cout << "Family already in the map" << std::endl;
	}
}


int main()
{
	std::multimap<std::string, std::string> FamilyChildrenMap;
	std::string Family, Child;

	std::cout << "Adding a child to a family" << std::endl;
	std::cout << "Insert the name of the family and then the name of the child or ctrl-z if you want to skip" << std::endl;
	while (std::cin >> Family && std::cin >> Child)
	{
		AddChild(FamilyChildrenMap, Family, Child);
		std::cout << "Insert the name of a new family and then the name of the child or ctrl-z if you want to skip" << std::endl;
	}


	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : FamilyChildrenMap)
	{
		std::cout << "Child " << Elem.second << " from family " << Elem.first << std::endl;
	}

	return 0;
}