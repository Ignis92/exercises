#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

bool Read(std::istream &Input, std::ostream &Output, SalesData &Target)
{
	Output << "Input the ISBN: ";
	Input >> Target.BookNumber;
	Output << "Input number of unit sold: ";
	Input >> Target.UnitSold;
	Output << "Input the revenue: ";
	Input >> Target.Revenue;
	if (Input)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Add(SalesData &Base, SalesData &ToAdd)
{
	Base.UnitSold += ToAdd.UnitSold;
	Base.Revenue += ToAdd.Revenue;
	return;
}

void Print(const SalesData &Target)
{
	std::cout << "Book ISBN: " << Target.BookNumber << std::endl;
	std::cout << "Books sold: " << Target.UnitSold << std::endl;
	std::cout << "Total revenue: " << Target.Revenue << std::endl;
}