// EXERCISE 5.11: Modify our vowel - counting program so that
// it also counts the number of blank spaces, tabs, and
// newlines read.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Line;

	std::cout << "Input a line: ";
	while (getline(std::cin, Line))
	{
		int vCnt = 0, sCnt = 0, oCnt = 0;
		for (const auto c : Line)
		{
			switch (toupper(c))
			{
			case 'A': case 'E': case 'I': case 'O': case 'U':
				++vCnt;
				break;
			case ' ': case '\t': case '\n':
				++sCnt;
				break;
			default:
				++oCnt;
				break;
			}
		}

		std::cout << "The number of vowels in the inserted line is: " << vCnt << std::endl;
		std::cout << "The number of space characters in the inserted line is: " << sCnt << std::endl;
		std::cout << "Input a line or ctrl-z to terminate: ";
	}

	return 0;
}