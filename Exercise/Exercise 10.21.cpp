// EXERCISE 10.21: Write a lambda that captures a local int
// variable and decrements that variable until it reaches 0.
// Once the variable is 0 additional calls should no longer
// decrement the variable. The lambda should return a bool
// that indicates whether the captured variable is 0.

#include "stdafx.h"
#include <iostream>

int main()
{
	int ValInt = 4;
	auto f = [&ValInt]() -> int // Unclear: does the execise want me to change the value of the int (so, reference)?
	{
		if (ValInt)
		{
			--ValInt;
		}
		return !ValInt;
	};

	for (unsigned I = 0; I != 10; ++I)
	{
		std::cout << I << " iteration. The boolean returned by lambda is: " << f() << std::endl;
	}

	return 0;
}