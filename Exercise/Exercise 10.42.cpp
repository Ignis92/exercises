// EXERCISE 10.42: Reimplement the program that
// eliminated duplicate words that we wrote in § 10.2.3 (p.
// 383) to use a list instead of a vector.

#include "stdafx.h"
#include <iostream>
#include <list>
#include <string>
#include <algorithm>

void ElimDups(std::list<std::string> &Target)
{
	Target.sort();
	Target.unique();
}

int main()
{
	std::list<std::string> ListStr{ "abc","def","abc","ghi","lmn","def","opq","abc" };

	ElimDups(ListStr);

	std::cout << "The list is:" << std::endl;
	for (const auto Elem : ListStr)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}