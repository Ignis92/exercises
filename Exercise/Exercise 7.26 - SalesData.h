// EXERCISE 7.26: Define Sales_data::avg_price as an inline
// function.

#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	// Friend
	friend SalesData Add(const SalesData &Data1, const SalesData &Data2);
	friend std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
	friend std::ostream &Print(std::ostream &Output, const SalesData &Target);

public:
	// Constructor
	SalesData() {}
	SalesData(const std::string S) : BookNumber(S) {}
	SalesData(const std::string S, unsigned U, double R) : BookNumber(S), UnitSold(U), Revenue(R) {}
	SalesData(std::istream & Input, std::ostream & Output);


	std::string ISBN() const { return BookNumber; }
	
	SalesData &Combine(const SalesData &ToAdd);
	double AvgPrice() const { return Revenue / UnitSold; }

private:
	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

SalesData Add(const SalesData &Data1, const SalesData &Data2);
std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
std::ostream &Print(std::ostream &Output, const SalesData &Target);