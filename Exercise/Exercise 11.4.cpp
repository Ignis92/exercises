// EXERCISE 11.4: Extend your program to ignore case
// and punctuation. For example, “example.” “example, ”
// and “Example” should all increment the same counter.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>
#include <algorithm>

int main()
{
	std::map<std::string, unsigned> WordCount;
	std::string Word;

	while (std::cin >> Word)
	{
		// Remove punctuation from the end
		auto Position = Word.find_last_of(",;.");
		while (Position == Word.size() - 1)
		{
			Word.erase(Position);
		}

		// Lowercase everything
		for (auto &C : Word)
		{
			C = tolower(C);
		}

		++WordCount[Word];
	}

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : WordCount)
	{
		std::cout << "The word \"" << Elem.first << "\" appears " << Elem.second << " time(s)." << std::endl;
	}

	return 0;
}