// EXERCISE 8.6: Rewrite the bookstore program from § 7.1.1
// (p. 256) to read its transactions from a file. Pass the name
// of the file as an argument to main(§ 6.2.5, p. 218).

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &InputFile)
{
	std::vector<std::string> Result;
	std::ifstream File(InputFile);

	std::string Line;
	while (File >> Line)
	{
		Result.push_back(Line);
	}

	return Result;
}

int main(int argv,char *argc[])
{
	std::ifstream Input(argc[1]);
	SalesData Total;

	if (Read(Input, std::cout, Total))
	{
		SalesData Tmp;
		while (Read(Input, std::cout, Tmp))
		{
			if (Total.ISBN() == Tmp.ISBN())
			{
				Add(Total, Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(std::cout, Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBN differs!" << std::endl;
		return -1;
	}
}