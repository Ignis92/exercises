// EXERCISE 3.22: Revise the loop that printed the first
// paragraph in text to instead change the elements in
// text that correspond to the first paragraph to all
// uppercase. After you�ve updated text, print its contents.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<std::string> Text;
	std::string Line;

	std::cout << "Input a line:" << std::endl;
	while (getline(std::cin, Line))
	{
		Text.push_back(Line);
		std::cout << "Input a line or ctrl-z to terminate:" << std::endl;
	}

	// first paragraph is always uppercase
	for (auto &c : *(Text.begin()))
	{
		c = toupper(c);
	}

	for (auto it = Text.begin() + 1; it != Text.end(); ++it)
	{
		if (*it == "")
		{
			for (auto &c : *(it + 1))
			{
				c = toupper(c);
			}
		}
	}

	for (auto it = Text.cbegin(); it != Text.cend(); ++it)
	{
		std::cout << *it << std::endl;
	}

	return 0;
}