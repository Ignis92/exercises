// EXERCISE 8.7: Revise the bookstore program from the
// previous section to write its output to a file. Pass the
// name of that file as a second argument to main.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &InputFile)
{
	std::vector<std::string> Result;
	std::ifstream File(InputFile);

	std::string Line;
	while (File >> Line)
	{
		Result.push_back(Line);
	}

	return Result;
}

int main(int argv,char *argc[])
{
	std::ifstream Input(argc[1]);
	std::ofstream Output(argc[2]);
	SalesData Total;

	if (Read(Input, Output, Total))
	{
		SalesData Tmp;
		while (Read(Input, Output, Tmp))
		{
			if (Total.ISBN() == Tmp.ISBN())
			{
				Add(Total, Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(Output, Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBN differs!" << std::endl;
		return -1;
	}
}