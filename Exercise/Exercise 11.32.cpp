// EXERCISE 11.32: Using the multimap from the previous
// exercise, write a program to print the list of authors and
// their works alphabetically.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <algorithm>
#include <string>
#include <list>

int main()
{
	std::multimap<std::string, std::string> AuthorToWorkMap;
	std::string Author, Work;

	std::cout << "Adding a work and its author" << std::endl;
	std::cout << "Insert the name of the work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	while (getline(std::cin, Work) && getline(std::cin, Author))
	{
		AuthorToWorkMap.insert({ Author, Work });
		std::cout << "Insert the name of a new work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	}

	std::cout << "The map is:" << std::endl;
	for (auto Iter = AuthorToWorkMap.begin(); Iter != AuthorToWorkMap.end();)
	{
		std::list<std::string> WorksToSort;
		auto Range = AuthorToWorkMap.upper_bound(Iter->first);
		std::cout << "Works from " << Iter->first << std::endl;

		for (; Iter != Range; ++Iter)
		{
			WorksToSort.push_back(Iter->second);
		}

		WorksToSort.sort();

		for (const auto &Elem : WorksToSort)
		{
			std::cout << "\t\t" << Elem << std::endl;
		}
	}

	return 0;
}