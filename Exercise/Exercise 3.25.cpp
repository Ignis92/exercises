// EXERCISE 3.25: Rewrite the grade clustering program from
// � 3.3.3 (p. 104) using iterators instead of subscripts.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<unsigned> Scores(11, 0);
	unsigned Grade;

	std::cout << "Input a grade:" << std::endl;
	while (std::cin >> Grade)
	{
		auto it = Scores.begin();
		++*(it + Grade / 10);
		std::cout << "Input a grade or ctrl-z to terminate:" << std::endl;
	}

	for (auto it = Scores.cbegin(); it != Scores.cend(); ++it)
	{
		std::cout << "Cluster number " << it - Scores.cbegin() << ": ";
		std::cout << *it << std::endl;
	}

	return 0;
}