// EXERCISE 9.45: Write a funtion that takes a string
// representing a name and two other strings representing
// a prefix, such as “Mr.” or “Ms.” and a suffix, such as “Jr.”
// or “III”. Using iterators and the insert and append
// functions, generate and return a new string with the
// suffix and prefix added to the given name.

#include "stdafx.h"
#include <iostream>
#include <string>

std::string &Complete(std::string &Name, const std::string &Prefix, const std::string &Suffix)
{
	auto Iter = Name.insert(Name.cbegin(), Prefix.cbegin(), Prefix.cend());
	Name.insert(Iter + Prefix.size(), ' ');
	Name.append(" " + Suffix);

	return Name;
}

int main()
{
	std::string Name("Henry Walton Jones"), Prefix("Dr."), Suffix("Jr.");

	std::cout << "String with prefix and suffix is:" << std::endl;
	std::cout << Complete(Name, Prefix, Suffix) << std::endl;

	return 0;
}