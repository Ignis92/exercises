// EXERCISE 9.14: Write a program to assign the elements
// from a list of char* pointers to C - style character strings to
// a vector of strings.

#include "stdafx.h"
#include <iostream>
#include <list>
#include <vector>
#include <string>

int main()
{
	std::list<char *> ListChar{ "Tomatoes", "Apples", "Salad" };
	std::vector<std::string> VectStr;

	auto Begin = ListChar.cbegin();
	auto End = ListChar.cend();

	VectStr.assign(Begin, End);

	for (auto Elem : VectStr)
	{
		std::cout << Elem << std::endl;
	}
	return 0;
}