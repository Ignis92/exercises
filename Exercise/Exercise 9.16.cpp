// EXERCISE 9.16: Repeat the previous program, but
// compare elements in a list<int> to a vector<int>.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <list>

int main()
{

	std::list<int> ListInt{ 1,2,3,4 };
	std::vector<int> Vect1{ 1,2,3,4 }, Vect2(ListInt.begin(),ListInt.end());

	if (Vect1 == Vect2)
	{
		std::cout << "Vectors are equal" << std::endl;
	}
	else
	{
		if (Vect1 > Vect2)
		{
			std::cout << "The first vector is greater" << std::endl;
		}
		else
		{
			std::cout << "The second vector is greater" << std::endl;
		}
	}

	return 0;
}