// EXERCISE 3.36: Write a program to compare two arrays
// for equality. Write a similar program to compare two
// vectors.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	// Arrays program
	/*
	constexpr int size = 10;
	int Arr1[] = { 1,5,7 }, Arr2[] = { 1,5,7,6 };

	// Array "contained" in the other one are considered smaller
	{
	bool Check = false;
	for (int i = 0; Check != true; ++i)
	{
	if (Arr1 + i == std::end(Arr1))
	{
	if (Arr2 + i == std::end(Arr2))
	{
	std::cout << "Arrays are equal." << std::endl;
	Check = true;
	}
	else
	{
	std::cout << "The second array is greater" << std::endl;
	Check = true;
	}
	}
	else
	{
	if (Arr2 + i == std::end(Arr2))
	{
	std::cout << "First array is greater" << std::endl;
	Check = true;
	}
	else
	{
	if (*(Arr1 + i) > *(Arr2 + i))
	{
	std::cout << "Second array is greater" << std::endl;
	Check = true;
	}
	else
	{
	if (*(Arr1 + i) < *(Arr2 + i))
	{
	std::cout << "First array is greater" << std::endl;
	Check = true;
	}
	}
	}
	}
	}
	}
	*/

	// Vectors program
	std::vector<int> Vect1{ 1,5,7 }, Vect2{ 1,5,7,3 };
	if (Vect1 == Vect2)
	{
		std::cout << "Vectors are equal" << std::endl;
	}
	else
	{
		if (Vect1 > Vect2)
		{
			std::cout << "First vector is greater" << std::endl;
		}
		else
		{
			std::cout << "Second vector is greater" << std::endl;
		}
	}
	return 0;
}