// EXERCISE 11.9: Define a map that associates words with
// a list of line numbers on which the word might occur.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <list>
#include <string>
#include <sstream>

int main()
{
	std::map<std::string, std::list<size_t>> WordOccurrences;
	std::string Line;
	size_t LineCounter = 1;

	std::cout << "Keep inserting strings or ctrl-z to terminate:" << std::endl;
	while (getline(std::cin, Line))
	{
		std::istringstream LineStream(Line);
		std::string Word;

		while (LineStream >> Word)
		{
			decltype(WordOccurrences.begin()) InsertPoint;
			if ((InsertPoint = WordOccurrences.find(Word)) == WordOccurrences.cend())
			{
				WordOccurrences.insert({ Word, {LineCounter} });
			}
			else
			{
				InsertPoint->second.push_back(LineCounter);
			}
		}

		++LineCounter;
	}

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : WordOccurrences)
	{
		std::cout << "the word \"" << Elem.first << "\" appears at the following line(s): ";
		for (const auto ListElem : Elem.second)
		{
			std::cout << ListElem << ",";
		}
		std::wcout << std::endl;
	}

	return 0;
}