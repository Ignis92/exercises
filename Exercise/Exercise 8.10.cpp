// EXERCISE 8.10: Write a program to store each line from
// a file in a vector<string>. Now use an istringstream to
// read each element from the vector a word at a time.

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

std::vector<std::string> &ReadFile(std::istream &Input,std::vector<std::string> &Target)
{
	std::string Line;
	while (getline(Input,Line))
	{
		Target.push_back(Line);
	}


	std::cin.clear();
	return Target;
}

int main()
{
	std::vector<std::string> Text;

	std::ifstream InputFile("C:\\Users\\abocco\\Desktop\\Lavori\\dizionarizzazione\\file u7p40\\me_10_ecap_hft_core.sas");
	ReadFile(InputFile, Text);

	for (auto it = Text.cbegin(); it != Text.cend(); ++it)
	{
		std::istringstream CurrLine(*it);
		std::string Word;
		while (CurrLine >> Word)
		{
			std::cout << Word << std::endl;
		}
	}

	return 0;
}