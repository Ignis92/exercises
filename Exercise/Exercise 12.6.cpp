// EXERCISE 12.6: Write a function that returns a
// dynamically allocated vector of ints. Pass that vector to
// another function that reads the standard input to give
// values to the elements. Pass the vector to another
// function to print the values that were read. Remember
// to delete the vector at the appropriate time.

#include "stdafx.h"
#include <iostream>
#include <new>
#include <vector>

std::vector<int> * CreateVector()
{
	return new std::vector<int>();
}

void AssignValues(std::vector<int> * P)
{
	for (size_t I = 0; I != 10; ++I)
	{
		P->push_back(I);
	}
	return;
}

void Print(std::vector<int> * P)
{
	std::cout << "The vector is:" << std::endl;
	for (auto Iter = P->cbegin(); Iter != P->cend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}
	return;
}


int main()
{
	auto P = CreateVector();
	Print(P);
	AssignValues(P);
	Print(P);
	delete P;

	return 0;
}