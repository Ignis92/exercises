// EXERCISE 11.33: Implement your own version of the
// word-transformation program.

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <sstream>

void ReadMap(std::map<std::string, std::string> &Target, std::ifstream &InputFile)
{
	std::string Key, Value;
	while (InputFile >> Key)
	{
		getline(InputFile, Value);
		if (Value.size() > 1)
		{
			Target[Key] = Value.substr(1);
		}
		else
		{
			throw std::runtime_error("Abbreviation without explicit value");
		}
	}

	return;
}

const std::string &Transform(const std::string &Str, std::map<std::string, std::string> &Interpret)
{
	auto Iter = Interpret.find(Str);
	if (Iter != Interpret.end())
	{
		return Iter->second;
	}
	else
	{
		return Str;
	}
}

void TextTransform(std::ifstream &TextToChange, std::ifstream &Abbreviations)
{
	std::map<std::string, std::string> AbbreviationsMap;
	ReadMap(AbbreviationsMap, Abbreviations);

	std::string Line;
	while (getline(TextToChange, Line))
	{
		std::istringstream LineStream(Line);
		std::string Word;
		while (LineStream >> Word)
		{
			std::cout << Transform(Word, AbbreviationsMap) << " ";
		}
		std::cout << std::endl;
	}

}

int main()
{
	std::ifstream TextToChange("Text.TXT"), Abbreviations("Abbreviations.TXT");
	TextTransform(TextToChange, Abbreviations);

	return 0;
}