// EXERCISE 6.23: Write your own versions of each of the
// print functions presented in this section. Call each of these
// functions to print i and j defined as follows:
// int i = 0, j[2] = { 0, 1 };

#include "stdafx.h"
#include <iostream>

void print(const int *Pointer)
{
	std::cout << "Print pointer to const int: " << *Pointer << std::endl;
}

void print(const int *beg, const int *end)
{
	while (beg != end)
	{
		std::cout << "Print beg-end: " << *beg++ << std::endl;
	}
}

void print(const int *arr, size_t size)
{
	for (size_t i = 0; i != size; ++i)
	{
		std::cout << "Print size: " << arr[i] << std::endl;
	}
}

int main()
{
	int i = 0, j[2] = { 0,1 };

	print(&i);
	print(j);
	print(std::begin(j), std::end(j));
	print(std::begin(j), std::end(j) - std::begin(j));

	return 0;
}