// EXERCISE 11.38: Rewrite the word - counting(§ 11.1, p.
// 421) and word - transformation(§ 11.3.6, p. 440)
// programs to use an unordered_map.

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <string>
#include <sstream>

void ReadMap(std::unordered_map<std::string, std::string> &Target, std::ifstream &InputFile)
{
	std::string Key, Value;
	while (InputFile >> Key)
	{
		getline(InputFile, Value);
		if (Value.size() > 1)
		{
			Target[Key] = Value.substr(1);
		}
		else
		{
			throw std::runtime_error("Abbreviation without explicit value");
		}
	}

	return;
}

const std::string &Transform(const std::string &Str, std::unordered_map<std::string, std::string> &Interpret)
{
	auto Iter = Interpret.find(Str);
	if (Iter != Interpret.end())
	{
		return Iter->second;
	}
	else
	{
		return Str;
	}
}

void TextTransform(std::ifstream &TextToChange, std::ifstream &Abbreviations)
{
	std::unordered_map<std::string, std::string> AbbreviationsMap;
	ReadMap(AbbreviationsMap, Abbreviations);

	std::string Line;
	while (getline(TextToChange, Line))
	{
		std::istringstream LineStream(Line);
		std::string Word;
		while (LineStream >> Word)
		{
			std::cout << Transform(Word, AbbreviationsMap) << " ";
		}
		std::cout << std::endl;
	}

}

int main()
{
	std::ifstream TextToChange("Text.TXT"), Abbreviations("Abbreviations.TXT");
	TextTransform(TextToChange, Abbreviations);

	return 0;
}