// EXERCISE 3.31: Write a program to define an array of ten
// ints. Give each element the same value as its position in
// the array.

#include "stdafx.h"
#include <iostream>

int main()
{
	int Arr[10];
	int Count = 0;

	for (auto &i : Arr)
	{
		i = Count;
		++Count;
	}

	std::cout << "The elements of the array are:" << std::endl;
	for (auto i : Arr)
	{
		std::cout << i << std::endl;
	}

	return 0;
}