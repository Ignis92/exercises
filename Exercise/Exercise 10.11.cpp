// EXERCISE 10.11: Write a program that uses stable_sort
// and isShorter to sort a vector passed to your version of
// elimDups. Print the vector to verify that your program is
// correct.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

bool isShorter(const std::string &Str1, const std::string &Str2)
{
	return Str1.size() < Str2.size();
}

void ElimDups(std::vector<std::string> &Target)
{
	sort(Target.begin(), Target.end());
	auto Tail = std::unique(Target.begin(), Target.end());
	Target.erase(Tail,Target.end());
}

int main()
{
	std::vector<std::string> VectStr{ "abc","defg","abc","hi","lmnop","defg","qrst","abc" };

	ElimDups(VectStr);
	stable_sort(VectStr.begin(), VectStr.end(), isShorter);

	std::cout << "The vector is:" << std::endl;
	for (const auto Elem : VectStr)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}