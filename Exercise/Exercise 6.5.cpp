// EXERCISE 6.5: Write a function to return the absolute value
// of its argument.

#include "stdafx.h"
#include <iostream>

double AbsValue(double N)
{
	return (N > 0) ? N : -N;
}

int main()
{
	int Num;

	std::cout << "Input a number: ";
	std::cin >> Num;

	std::cout << "The absolute value of " << Num << " is " << AbsValue(Num) << std::endl;

	return 0;
}