#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	std::string ISBN() const { return BookNumber; }
	SalesData &Combine(const SalesData &ToAdd)
	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

SalesData &SalesData::Combine(const SalesData &ToAdd)
{
	UnitSold += ToAdd.UnitSold;
	Revenue += ToAdd.Revenue;
	return *this;
}

bool Read(std::istream &Input, std::ostream &Output, SalesData &Target)
{
	Output << "Input the ISBN: ";
	Input >> Target.BookNumber;
	Output << "Input number of unit sold: ";
	Input >> Target.UnitSold;
	Output << "Input the revenue: ";
	Input >> Target.Revenue;
	if (Input)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Print(const SalesData &Target)
{
	std::cout << "Book ISBN: " << Target.BookNumber << std::endl;
	std::cout << "Books sold: " << Target.UnitSold << std::endl;
	std::cout << "Total revenue: " << Target.Revenue << std::endl;
}
