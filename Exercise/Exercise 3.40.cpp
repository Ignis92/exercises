// EXERCISE 3.40: Write a program to define two character
// arrays initialized from string literals. Now define a third
// character array to hold the concatenation of the two arrays.
// Use strcpy and strcat to copy the two arrays into the third.

#include "stdafx.h"
#include <iostream>

int main()
{
	const char Arr1[] = "Ciao", Arr2[] = "Mondo";
	std::cout << "First string: " << Arr1 << std::endl;
	std::cout << "Second string: " << Arr2 << std::endl;


	char ArrConc[std::size(Arr1) + std::size(Arr2)];

	strcpy_s(ArrConc, Arr1);
	strcat_s(ArrConc, Arr2);
	std::cout << "Concatenated string: " << ArrConc << std::endl;

	return 0;
}