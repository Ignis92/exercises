// EXERCISE 3.16: Write a program to print the size and
// contents of the vectors from exercise 3.13. Check
// whether your answers to that exercise were correct. If
// not, restudy � 3.3.1 (p. 97) until you understand why
// you were wrong.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{

	std::vector<int> v1, v2(10), v3(10, 42), v4{ 10 }, v5{ 10,42 };
	std::vector<std::string> v6{ 10 }, v7{ 10,"hi" };

	std::vector<std::vector<int>> VectInt{ v1,v2,v3,v4,v5 };
	std::vector<std::vector<std::string>> VectStr{ v6,v7 };

	for (auto Vect : VectInt)
	{
		std::cout << "Length of vector: " << Vect.size() << std::endl;
		std::cout << "Elements of vector:" << std::endl;
		for (auto Elem : Vect)
		{
			std::cout << Elem << " ";
		}
		std::cout << std::endl << std::endl;
	}

	for (auto Vect : VectStr)
	{
		std::cout << "Length of vector: " << Vect.size() << std::endl;
		std::cout << "Elements of vector:" << std::endl;
		for (auto Elem : Vect)
		{
			std::cout << Elem << " ";
		}
		std::cout << std::endl << std::endl;
	}

	return 0;
}