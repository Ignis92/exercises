// EXERCISE 10.24: Use bind and check_size to find the
// first element in a vector of ints that has a value greater
// than the length of a specified string value.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>

bool Compare(int ValInt, const std::string &Str)
{
	return ValInt > Str.size();
}

int main()
{
	std::vector<int> VectInt{ 1, 2, 5, 7, 10 };
	std::string String;

	std::cout << "Input a word: ";
	std::cin >> String;

	auto Check = std::bind(Compare, std::placeholders::_1, String);
	auto Iter = find_if(VectInt.cbegin(), VectInt.cend(), Check);

	if (Iter == VectInt.cend())
	{
		std::cout << "No element of the vector is big enough." << std::endl;
	}
	else
	{
		std::cout << "Element " << Iter - VectInt.cbegin() + 1;
		std::cout << " is the first element greater than the size of the input string." << std::endl;
	}

	return 0;
}