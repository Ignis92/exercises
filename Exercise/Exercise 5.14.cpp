// EXERCISE 5.14: Write a program to read strings from
// standard input looking for duplicated words. The
// program should find places in the input where one word
// is followed immediately by itself. Keep track of the
// largest number of times a single repetition occurs and
// which word is repeated. Print the maximum number of
// duplicates, or else print a message saying that no word
// was repeated. For example, if the input is
// how now now now brown cow cow
// the output should indicate that the word now occurred three
// times.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Line;

	std::cout << "Input a line:" << std::endl;
	while (getline(std::cin, Line))
	{
		int MaxCnt = 0, CurrCnt = 1;
		std::string MaxWord, PrevWord, CurrWord;
		auto it = Line.cbegin();
		while (it != Line.cend())
		{
			if (isalpha(*it))
			{
				CurrWord += *it;
			}
			if (!isalpha(*it) || it + 1 == Line.cend())
			{
				if (PrevWord != CurrWord)
				{
					if (CurrCnt > MaxCnt)
					{
						MaxCnt = CurrCnt;
						MaxWord = (PrevWord.length()) ? PrevWord : CurrWord;
					}
					CurrCnt = 1;
				}
				else
				{
					++CurrCnt;
					if (it + 1 == Line.cend())
					{
						if (CurrCnt > MaxCnt)
						{
							MaxCnt = CurrCnt;
							MaxWord = (PrevWord.length()) ? PrevWord : CurrWord;
						}
					}
				}
				PrevWord = CurrWord;
				CurrWord.erase();
			}
			++it;
		}

		if (MaxCnt == 1)
		{
			std::cout << "No word was repeated." << std::endl;
		}
		else
		{
			std::cout << "The most duplicated word is " << MaxWord << ", with " << MaxCnt << " repetitions." << std::endl;
		}
		std::cout << "Input a line or ctrl-z to terminate:" << std::endl;
	}

	return 0;
}