// EXERCISE 6.22: Write a function to swap two int pointers.

#include "stdafx.h"
#include <iostream>

void Swap(int *&Pointer1, int *&Pointer2)
{
	int *Tmp = Pointer2;
	Pointer2 = Pointer1;
	Pointer1 = Tmp;
}

int main()
{
	int PointedInt1 = 7, PointedInt2 = 3;
	int *Pointer1 = &PointedInt1, *Pointer2 = &PointedInt2;

	std::cout << "Before swapping:" << std::endl;
	std::cout << "First pointer: " << Pointer1 << ". Second pointer: " << Pointer2 << "." << std::endl;

	Swap(Pointer1, Pointer2);

	std::cout << "After swapping:" << std::endl;
	std::cout << "First pointer: " << Pointer1 << ". Second pointer: " << Pointer2 << "." << std::endl;

	return 0;
}