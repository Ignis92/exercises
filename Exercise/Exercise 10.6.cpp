// EXERCISE 10.6: Using fill_n, write a program to set a
// sequence of int values to 0.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
	std::vector<int> VectInt{ 1,2,3,4,5,6,7,8,9 };

	std::fill_n(VectInt.begin(), VectInt.size(), 0);

	std::cout << "The vector is:" << std::endl;
	for (const auto Elem : VectInt)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}