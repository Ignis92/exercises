// EXERCISE 3.39: Write a program to compare two strings.
// Now write a program to compare the values of two C - style
// character strings.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	// Strings program
	/*
	std::string Str1 = "ciao", Str2 = "mondo";

	if (Str1 == Str2)
	{
	std::cout << "Strings are equal" << std::endl;
	}
	else
	{
	if (Str1 > Str2)
	{
	std::cout << "First string is greater" << std::endl;
	}
	else
	{
	std::cout << "Second string is greater" << std::endl;
	}
	}
	*/

	// C-style strings
	const char CStr1[] = "ciao", CStr2[] = "mondo";
	int Result = strcmp(CStr1, CStr2);

	if (Result == 0)
	{
		std::cout << "Strings are equal." << std::endl;
	}
	else
	{
		if (Result > 0)
		{
			std::cout << "First string is greater." << std::endl;
		}
		else
		{
			std::cout << "Second string is greater." << std::endl;
		}
	}

	return 0;
}