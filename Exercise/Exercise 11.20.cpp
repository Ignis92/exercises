// EXERCISE 11.20: Rewrite the word-counting program
// from § 11.1 (p. 421) to use insert instead of
// subscripting. Which program do you think is easier to
// write and read? Explain your reasoning.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>

int main()
{
	std::map<std::string, size_t> WordCount;
	std::string Word;
	while (std::cin >> Word)
	{
		// From the following exercise: ++WordCount.insert({ Word, 0 }).first->second;
		// it works because insert is not like subscript, that is it doesn't
		// overwrite a value if already present in the map

		if (WordCount.find(Word) == WordCount.end())
		{
			WordCount.emplace(Word, 1);
		}
		else
		{
			++WordCount[Word];
		}
	}

	for (const auto &W : WordCount)
	{
		std::cout << W.first << " occurs " << W.second << ((W.second > 1) ? " times" : " time") << std::endl;
	}

	return 0;
}