// EXERCISE 3.43: Write three different versions of a
// program to print the elements of ia. One version should
// use a range for to manage the iteration, the other two
// should use an ordinary for loop in one case using
// subscripts and in the other using pointers. In all three
// programs write all the types directly. That is, do not use
// a type alias, auto, or decltype to simplify the code.

#include "stdafx.h"
#include <iostream>

int main()
{
	int ia[3][4] = { { 1,2,3,4 },{ 5,6,7,8 },{ 9,10,11,12 } };

	std::cout << "The bidimensional array is:" << std::endl;

	// Range for
	for (int(&i)[std::size(ia[0])] : ia)
	{
		for (int j : i)
		{
			std::cout << j << " ";
		}
		std::cout << std::endl;
	}

	// Subscripts
	for (int i = 0; i < std::size(ia); ++i)
	{
		for (int j = 0; j < std::size(ia[0]); ++j)
		{
			std::cout << ia[i][j] << " ";
		}
		std::cout << std::endl;
	}

	// Pointers
	for (int (*i)[std::size(ia[0])] = std::begin(ia); i != std::end(ia); ++i)
	{
		for (int *j = std::begin(*i); j != std::end(*i); ++j)
		{
			std::cout << *j << " ";
		}
		std::cout << std::endl;
	}

	return 0;
}