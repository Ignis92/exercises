// EXERCISE 3.4: Write a program to read two strings and
// report whether the strings are equal. If not, report
// which of the two is larger. Now, change the program to
// report whether the strings have the same length, and if
// not, report which is longer.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Str1, Str2;
	std::cout << "Input the first string:" << std::endl;
	getline(std::cin, Str1);
	std::cout << "Input the second string:" << std::endl;
	getline(std::cin, Str2);

	if (Str1.size() == Str2.size()) // delete both .size() to compare strings
	{
		std::cout << "The two strings are equal" << std::endl;
	}
	else
	{
		if (Str1.size() < Str2.size()) // delete both .size() to compare strings
		{
			std::cout << "The second string is greater than the first one" << std::endl;
		}
		else
		{
			std::cout << "The first string is greater than the second one" << std::endl;
		}
	}

	return 0;
}