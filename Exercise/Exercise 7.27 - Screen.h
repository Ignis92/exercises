#pragma once

#include <string>

class Screen
{
public:
	using Pos = std::string::size_type;

	// Constructor
	Screen() = default;
	Screen(Pos H, Pos W) : Height(H), Width(W), Contents(H * W, ' ') {}
	Screen(Pos H, Pos W, char C) : Height(H), Width(W), Contents(H * W, C) {}

	// Get
	char GetChar() const { return Contents[CursorPosition]; }
	char GetChar(Pos Row, Pos Column) const { return Contents[Row * Width + Column]; }

	Screen &Move(Pos Row, Pos Column);
	Screen &Set(char ToChange);
	Screen &Set(Pos Row, Pos Column, char ToChange);
	Screen &Display(std::ostream &Output);
	const Screen &Display(std::ostream &Output) const;

private:
	void DoDisplay(std::ostream &Output) const;
	std::string Contents;
	Pos Height = 0, Width = 0;
	Pos CursorPosition = 0;
};