// EXERCISE 9.28: Write a function that takes a
// forward_list<string> and two additional string
// arguments. The function should find the first string and
// insert the second immediately following the first. If the
// first string is not found, then insert the second string at
// the end of the list.

#include "stdafx.h"
#include <iostream>
#include <forward_list>
#include <string>

int main()
{
	std::forward_list<std::string> StringList{ "Hello","world","what","a","day" };
	std::string StrToFind = "don't", StrToInsert = "lovely";
	{
		auto CurrIter = StringList.cbegin(), ForwardIter = ++StringList.cbegin();
		while (ForwardIter != StringList.cend() && StrToFind != *CurrIter)
		{
			CurrIter = ForwardIter;
			++ForwardIter;
		}
		StringList.insert_after(CurrIter, StrToInsert);
	}

	std::cout << "The list of string is now:" << std::endl;
	for (const std::string &Str : StringList)
	{
		std::cout << Str << std::endl;
	}

	return 0;
}