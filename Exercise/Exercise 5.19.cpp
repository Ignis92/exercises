// EXERCISE 5.19: Write a program that uses a do while loop
// to repetitively request two strings from the user and report
// which string is less than the other.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Str1, Str2, Answer;

	do
	{
		std::cout << "Input the first string:" << std::endl;
		getline(std::cin, Str1);
		std::cout << "Input the second string:" << std::endl;
		getline(std::cin, Str2);

		if (Str1 == Str2)
		{
			std::cout << "Strings are the same." << std::endl;
		}
		else
		{
			if (Str1 < Str2)
			{
				std::cout << "The first string is less than the second one." << std::endl;
			}
			else
			{
				std::cout << "The second string is less than the first one." << std::endl;
			}
		}

		std::cout << "Continue? ";
		std::cin >> Answer;
		std::cin.ignore();
	} while (Answer != "n");

	return 0;
}