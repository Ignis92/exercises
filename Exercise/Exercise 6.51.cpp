// EXERCISE 6.51: Write all four versions of f. Each
// function should print a distinguishing message. Check
// your answers for the previous exercise. If your answers
// were incorrect, study this section until you understand
// why your answers were wrong.

#include "stdafx.h"
#include <iostream>

void f(void)
{
	std::cout << "Executing f(void)." << std::endl;
}

void f(int)
{
	std::cout << "Executing f(int)." << std::endl;
}

void f(int, int)
{
	std::cout << "Executing f(int, int)." << std::endl;
}

void f(double, double = 3.14)
{
	std::cout << "Executing f(double, double)." << std::endl;
}
int main()
{
	f(2.56, 42);
	f(42);
	f(42, 0);
	f(2.56, 3.14);

	return 0;
}