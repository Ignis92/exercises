// EXERCISE 7.41: Rewrite your own version of the
// Sales_data class to use delegating constructors. Add a
// statement to the body of each of the constructors that
// prints a message whenever it is executed. Write
// declarations to construct a Sales_data object in every
// way possible. Study the output until you are certain you
// understand the order of execution among delegating
// constructors.


#include "stdafx.h"
#include <iostream>
#include "SalesData.h"

int main()
{
	SalesData A;
	std::cout << std::endl;
	SalesData B("99-9999");
	std::cout << std::endl;
	SalesData C("99-9999", 15, 78.4);
	std::cout << std::endl;
	SalesData D(std::cin, std::cout);
	std::cout << std::endl;
	return 0;
}