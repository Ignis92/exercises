// EXERCISE 12.14: Write your own version of a function
// that uses a shared_ptr to manage a connection.

#include "stdafx.h"
#include <iostream>
#include <memory>

class Destination {};

class Connection
{
public:
	Connection(Destination*) {}
};

void EndConnection(Connection * C)
{
	delete(C);
	return;
}

void F(Destination &D)
{
	Connection Conn(&D);
	std::shared_ptr<Connection> PC(&Conn, EndConnection);

	// Use the connection

	return;
}
int main()
{
	Destination D;
	F(D);
	return 0;
}