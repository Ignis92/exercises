// EXERCISE 9.5: Rewrite the previous program to return
// an iterator to the requested element. Note that the
// program must handle the case where the element is not
// found.

#include "stdafx.h"
#include <iostream>
#include <vector>

std::vector<int>::const_iterator LookforValue(std::vector<int>::const_iterator Start,
	std::vector<int>::const_iterator End, int Value)
{
	while (Start != End && Value != *Start)
	{
		++Start;
	}

	return Start;
}

int main()
{
	std::vector<int> VectInt{ 1,3,64,7,2,10,5682,123,5,323 };

	auto Start = VectInt.begin();
	auto End = VectInt.end();

	auto Result = LookforValue(Start, End, 7);

	if (Result != End)
	{
		std::cout << "Found it at position " << Result - VectInt.cbegin() + 1 << "!" << std::endl;
	}
	else
	{
		std::cout << "Value not found!" << std::endl;
	}

	return 0;
}