// EXERCISE 10.12: Write a function named compareIsbn
// that compares the isbn() members of two Sales_data
// objects. Use that function to sort a vector that holds
// Sales_data objects.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include "SalesData.h"

bool compareISBN(const SalesData &Obj1, const SalesData &Obj2)
{
	return Obj1.ISBN().size() < Obj2.ISBN().size();
}

int main()
{
	std::vector<SalesData> VectSalesData{ SalesData("IT10",5,28),SalesData("SPA15",3,15),SalesData("D1",4,25) };

	sort(VectSalesData.begin(), VectSalesData.end(), compareISBN);

	std::cout << "The vector is:" << std::endl;
	for (const auto Elem : VectSalesData)
	{
		Print(std::cout, Elem);
	}

	return 0;
}