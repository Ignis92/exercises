// EXERCISE 11.7: Define a map for which the key is the
// family’s last name and the value is a vector of the
// children’s names. Write code to add new families and to
// add new children to an existing family.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

void AddChild(std::map<std::string, std::vector<std::string>> &Target, const std::string &Family, const std::string &NewChild)
{
	auto Iter = Target.find(Family);
	if (Iter == Target.end())
	{
		std::cout << "Family not found" << std::endl;
	}
	else
	{
		Iter->second.push_back(NewChild);
	}
}

void AddFamily(std::map<std::string, std::vector<std::string>> &Target, const std::string &NewFamily,
	const std::vector<std::string> &Children)
{
	auto Iter = Target.find(NewFamily);
	if (Iter == Target.end())
	{
		Target.insert({ NewFamily,Children });
		return;
	}
	else
	{
		std::cout << "Family already in the map" << std::endl;
	}
}


int main()
{
	std::map<std::string, std::vector<std::string>> FamilyChildrenMap;
	std::string Family, Child;
	std::vector<std::string> Children;

	std::cout << "FIRST SECTION: adding a whole family" << std::endl;
	std::cout << "Insert the name of the family or ctrl-z if you want to skip" << std::endl;
	while (std::cin >> Family)
	{
		Children.clear();
		std::cout << "Insert the name of a child or ctrl-z if you want to skip" << std::endl;
		while (std::cin >> Child)
		{
			Children.push_back(Child);
			std::cout << "Insert the name of an additional child or ctrl-z if you want to skip" << std::endl;
		}
		AddFamily(FamilyChildrenMap, Family, Children);

		std::cin.clear();
		std::cout << "Insert the name of a new family or ctrl-z if you want to skip" << std::endl;
	}

	std::cin.clear();
	std::cout << "SECOND SECTION: adding a child to a family" << std::endl;
	std::cout << "Insert the name of the family and then the name of the child or ctrl-z if you want to skip" << std::endl;
	while (std::cin >> Family && std::cin >> Child)
	{
		AddChild(FamilyChildrenMap, Family, Child);
		std::cout << "Insert the name of a new family and then the name of the child or ctrl-z if you want to skip" << std::endl;
	}


	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : FamilyChildrenMap)
	{
		std::cout << "The family " << Elem.first << " has the following children:" << std::endl;
		for (const auto &ChildElem : Elem.second)
		{
			std::cout << ChildElem << std::endl;
		}
	}

	return 0;
}