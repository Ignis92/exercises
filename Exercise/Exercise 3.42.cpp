// EXERCISE 3.42: Write a program to copy a vector of
// ints into an array of ints.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	constexpr int Size = 10; // hope it works, there is no other way to initialize the array
	std::vector<int> Vect{ 1,2,3,4,5,6,7,8,9,10 };
	int Arr[Size];

	for (int i = 0; i != Size && i < Vect.size(); ++i)
	{
		Arr[i] = Vect[i];
	}

	std::cout << "The array has the following elements:" << std::endl;
	for (auto i : Arr)
	{
		std::cout << i << std::endl;
	}


	return 0;
}