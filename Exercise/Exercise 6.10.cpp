// EXERCISE 6.10: Using pointers, write a function to swap
// the values of two ints. Test the function by calling it and
// printing the swapped values.

#include "stdafx.h"
#include <iostream>

// This swaps ints: what about performance comparison with swapping pointers?
void Swap(int *a, int *b)
{
	int tmp = *b;
	*b = *a;
	*a = tmp;

	return;
}

int main()
{
	int Int1 = 5, Int2 = 10;

	std::cout << "Before swapping:" << std::endl;
	std::cout << "First integer: " << Int1 << ". Second Integer: " << Int2 << "." << std::endl;

	Swap(&Int1, &Int2);

	std::cout << "After swapping:" << std::endl;
	std::cout << "First integer: " << Int1 << ". Second Integer: " << Int2 << "." << std::endl;

	return 0;
}