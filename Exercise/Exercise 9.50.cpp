// EXERCISE 9.50: Write a program to process a
// vector<string>s whose elements represent integral
// values. Produce the sum of all the elements in that
// vector. Change the program so that it sums of strings
// that represent floating - point values.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>

int main()
{
	std::vector<std::string> VectStr{ "12","34","2" };
	int Sum = 0;
	for (auto Elem : VectStr)
	{
		Sum += std::stoi(Elem);
	}

	std::cout << "Sum is: " << Sum << std::endl;
	return 0;
}