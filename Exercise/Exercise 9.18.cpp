// EXERCISE 9.18: Write a program to read a sequence of
// strings from the standard input into a deque. Use
// iterators to write a loop to print the elements in the
// deque.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <deque>

int main()
{

	std::string Input;
	std::deque<std::string> Result;

	std::cout << "Input a string:" << std::endl;
	while (getline(std::cin,Input))
	{
		Result.push_back(Input);
		std::cout << "Input a string or ctrl-z to terminate" << std::endl;
	}

	for (auto Iter = Result.cbegin(); Iter != Result.cend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}

	return 0;
}