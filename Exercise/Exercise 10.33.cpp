// EXERCISE 10.33: Write a program that takes the names of
// an input file and two output files. The input file should hold
// integers. Using an istream_iterator read the input file. Using
// ostream_iterators, write the odd numbers into the first
// output file. Each value should be followed by a space. Write
// the even numbers into the second file. Each of these values
// should be placed on a separate line.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <fstream>
#include <string>

int main()
{
	std::string StrInput, StrOutputOdd, StrOutputEven;
	
	std::cout << "Insert the input file name: " << std::endl;
	std::cin >> StrInput;
	std::cout << "Insert the file name to output odd numbers: " << std::endl;
	std::cin >> StrOutputOdd;
	std::cout << "Insert the file name to output even numbers: " << std::endl;
	std::cin >> StrOutputEven;

	std::ifstream InputFile(StrInput);
	std::ofstream OutputFileOdd(StrOutputOdd), OutputFileEven(StrOutputEven);
	std::istream_iterator<int> IterInput(InputFile), EndOfFile;
	std::ostream_iterator<int> IterOutputOdd(OutputFileOdd,"\n"), IterOutputEven(OutputFileEven,"\n");

	while (IterInput != EndOfFile)
	{
		if (*IterInput % 2)
		{
			IterOutputOdd = *IterInput++;
		}
		else
		{
			IterOutputEven = *IterInput++;
		}
	}

	return 0;
}