// EXERCISE 3.15: Repeat the previous program but read
// strings this time.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<std::string> Vector;
	std::string NewElement;

	std::cout << "Input a string:" << std::endl;
	while (getline(std::cin, NewElement))
	{
		Vector.push_back(NewElement);
		std::cout << "Input a string or ctrl-z to end:" << std::endl;
	}

	std::cout << "Final vector:" << std::endl;

	for (auto i : Vector)
	{
		std::cout << i << std::endl;
	}

	return 0;
}