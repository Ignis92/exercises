// EXERCISE 9.44: Rewrite the previous function using an
// index and replace.

#include "stdafx.h"
#include <iostream>
#include <string>

std::string &Substitute(std::string &s, const std::string &oldVal, const std::string &newVal)
{
	for (std::string::size_type i=0; i!=s.size(); ++i)
	{
		std::string::size_type j = 0, WordStartInd = i;
		while (j != oldVal.size() && i != s.size() && s[i] == oldVal[j])
		{
			++i;
			++j;
		}

		if (j == oldVal.size())
		{
			s.replace(WordStartInd, oldVal.size(), newVal);
			i = WordStartInd + newVal.size();
		}
	}

	return s;
}

int main()
{
	std::string s("Tho this, we got there. It is good, Tho could be better"), oldVal("Tho"), newVal("Though");

	std::cout << "String after correction is:" << std::endl;
	std::cout << Substitute(s, oldVal, newVal) << std::endl;

	return 0;
}