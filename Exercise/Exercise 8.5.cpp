// EXERCISE 8.5: Rewrite the previous program to store each
// word in a separate element.

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &InputFile)
{
	std::vector<std::string> Result;
	std::ifstream File(InputFile);

	std::string Line;
	while (File >> Line)
	{
		Result.push_back(Line);
	}

	return Result;
}

int main(int argv,char *argc[])
{
	std::vector<std::string> Text;
	Text = ReadFile(argc[1]);

	for (auto l : Text)
	{
		std::cout << l << std::endl;
	}

	return 0;
}