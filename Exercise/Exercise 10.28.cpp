// EXERCISE 10.28: Copy a vector that holds the values from 1
// to 9 inclusive, into three other containers. Use an inserter, a
// back_inserter, and a front_inserter, respectivly to add
// elements to these containers. Predict how the output
// sequence varies by the kind of inserter and verify your
// predictions by running your programs.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>

int main()
{
	std::vector<int> VectOrig{ 0,1,2,3,4,5,6,7,8,9 };
	std::list<int> ListInt;
	std::deque<int> DequeInt;
	std::vector<int> VectInt;

	std::copy(VectOrig.cbegin(), VectOrig.cend(), back_inserter(VectInt));
	std::copy(VectOrig.cbegin(), VectOrig.cend(), front_inserter(DequeInt));
	std::copy(VectOrig.cbegin(), VectOrig.cend(), inserter(ListInt, ListInt.end()));

	std::cout << "The copied vector is:" << std::endl;
	for (const auto Elem : VectInt)
	{
		std::cout << Elem << std::endl;
	}

	std::cout << "The copied deque is:" << std::endl;
	for (const auto Elem : DequeInt)
	{
		std::cout << Elem << std::endl;
	}

	std::cout << "The copied list is:" << std::endl;
	for (const auto Elem : ListInt)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}