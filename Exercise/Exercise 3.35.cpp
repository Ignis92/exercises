// EXERCISE 3.35: Using pointers, write a program to set
// the elements in an array to zero.

#include "stdafx.h"
#include <iostream>

int main()
{
	constexpr int size = 10;
	int Arr[size];

	for (int* p = Arr; p != std::end(Arr); ++p)
	{
		*p = 0;
	}

	std::cout << "The array elements are:" << std::endl;
	for (auto i : Arr)
	{
		std::cout << i << std::endl;
	}

	return 0;
}