// EXERCISE 7.27: Add the move, set, and display operations to
// your version of Screen. Test your class by executing the
// following code:
// Screen myScreen(5, 5, 'X');
// myScreen.move(4, 0).set('#').display(cout);
// cout << "\n";
// myScreen.display(cout);
// cout << "\n";

#include "stdafx.h"
#include "Screen.h"
#include <iostream>


int main()
{
	Screen myScreen(5, 5, 'X');
	myScreen.Move(4, 0).Set('#').Display(std::cout);
	std::cout << "\n";
	myScreen.Display(std::cout);
	std::cout << "\n";
	return 0;
}