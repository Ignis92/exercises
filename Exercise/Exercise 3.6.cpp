// EXERCISE 3.6: Use a range for to change all the characters
// in a string to X.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Str("Hello");
	std::cout << "Starting string:" << std::endl;
	std::cout << Str << std::endl;
	for (auto &c : Str)
	{
		c = 'X';
	}

	std::cout << "Final string:" << std::endl;
	std::cout << Str << std::endl;
	return 0;
}