// EXERCISE 10.36: Use find to find the last element in a list
// of ints with value 0.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <list>

int main()
{
	std::list<int> ListInt{ 0,1,2,0,3,4,5,0,6,7,8,0,9 };

	auto Iter = std::find(ListInt.crbegin(), ListInt.crend(), 0);
	std::cout << "The last occurence of 0 is at element " << &Iter << std::endl;
	return 0;
}