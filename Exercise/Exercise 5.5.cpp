// EXERCISE 5.5: Using an if�else statement, write your
// own version of the program to generate the letter grade
// from a numeric grade.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	unsigned int Score;
	char *IntGrade[10] = { "1","2","3","4","5","6","7","8","9","10" };
	std::string FinalGrade;

	std::cout << "Input a score: ";
	std::cin >> Score;

	if (Score < 60)
	{
		FinalGrade = "Insufficient";
	}
	else
	{
		FinalGrade = IntGrade[Score / 10 - 1];
		int Rest = Score % 10;
		if (Rest > 2)
		{
			if (Rest == 3 || Rest == 4)
			{
				FinalGrade += "+";
			}
			else
			{
				if (Rest == 5 || Rest == 6)
				{
					FinalGrade += " and half";
				}
				else
				{
					if (Rest == 7 || Rest == 8)
					{
						FinalGrade = FinalGrade + "/" + IntGrade[Score / 10];
					}
					else
					{
						FinalGrade = IntGrade[Score / 10];
						FinalGrade += "-";
					}
				}
			}
		}
	}
	std::cout << FinalGrade << std::endl;

	return 0;
}