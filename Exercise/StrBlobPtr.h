// EXERCISE 12.19: Define your own version of StrBlobPtr and
// update your StrBlob class with the appropriate friend
// declaration and begin and end members.

#pragma once

#include <memory>
#include <vector>

class StrBlob;

class StrBlobPtr
{
public:
	StrBlobPtr() : Curr(0) {}
	// StrBlobPtr(StrBlob &S,size_t Size): WPtr(S.Data), Curr(Size) {}
	// std::string & Deref() const { return (};
	StrBlobPtr & Incr();

private:
	std::weak_ptr<std::vector<std::string>> WPtr;
	std::size_t Curr;
};

