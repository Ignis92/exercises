// EXERCISE 9.46: Rewrite the previous exercise using a
// position and length to manage the strings. This time
// use only the insert function.

#include "stdafx.h"
#include <iostream>
#include <string>

std::string &Complete(std::string &Name, const std::string &Prefix, const std::string &Suffix)
{
	Name.insert(0, Prefix + " ");
	Name.insert(Name.size(), " " + Suffix);

	return Name;
}

int main()
{
	std::string Name("Henry Walton Jones"), Prefix("Dr."), Suffix("Jr.");

	std::cout << "String with prefix and suffix is:" << std::endl;
	std::cout << Complete(Name, Prefix, Suffix) << std::endl;

	return 0;
}