// EXERCISE 3.21: Redo the first exercise from � 3.3.3 (p.
// 105) using iterators.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<int> v1, v2(10), v3(10, 42), v4{ 10 }, v5{ 10,42 };
	std::vector<std::string> v6{ 10 }, v7{ 10,"hi" };

	std::vector<std::vector<int>> VectInt{ v1,v2,v3,v4,v5 };
	std::vector<std::vector<std::string>> VectStr{ v6,v7 };

	for (auto it1 = VectInt.cbegin(); it1 != VectInt.cend(); ++it1)
	{
		std::cout << "Length of vector: " << it1->size() << std::endl;
		std::cout << "Elements of vector:" << std::endl;
		for (auto it2 = it1->cbegin(); it2 != it1->cend(); ++it2)
		{
			std::cout << *it2 << " ";
		}
		std::cout << std::endl << std::endl;
	}

	for (auto it1 = VectStr.cbegin(); it1 != VectStr.cend(); ++it1)
	{
		std::cout << "Length of vector: " << it1->size() << std::endl;
		std::cout << "Elements of vector:" << std::endl;
		for (auto it2 = it1->cbegin(); it2 != it1->cend(); ++it2)
		{
			std::cout << *it2 << " ";
		}
		std::cout << std::endl << std::endl;
	}


	return 0;
}