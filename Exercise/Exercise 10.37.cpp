// EXERCISE 10.37: Given a vector that has ten elements,
// copy the elements from positions 3 through 7 in reverse
// order to a list.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>

int main()
{
	std::vector<int> VectInt{ 0,1,2,3,4,5,6,7,8,9 };
	std::list<int> ListInt;

	std::copy(VectInt.crbegin() + 2, VectInt.crend() - 3, std::inserter(ListInt,ListInt.end()));

	std::cout << "Printing list element:" << std::endl;
	for (const auto Elem : ListInt)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}