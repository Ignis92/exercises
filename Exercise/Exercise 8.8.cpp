// EXERCISE 8.8: Revise the program from the previous
// exercise to append its output to its given file. Run the
// program on the same output file at least twice to ensure
// that the data are preserved.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &InputFile)
{
	std::vector<std::string> Result;
	std::ifstream File(InputFile);

	std::string Line;
	while (File >> Line)
	{
		Result.push_back(Line);
	}

	return Result;
}

int main(int argv, char *argc[])
{
	std::ifstream Input(argc[1]);
	std::ofstream Output(argc[2], ofstream::app);
	SalesData Total;

	if (Read(Input, Output, Total))
	{
		SalesData Tmp;
		while (Read(Input, Output, Tmp))
		{
			if (Total.ISBN() == Tmp.ISBN())
			{
				Add(Total, Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(Output, Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBN differs!" << std::endl;
		return -1;
	}
}