// EXERCISE 9.38: Write a program to explore how vectors
// grow in the library you use.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> VectInt;

	std::cout << "Initial capacity at empty vector: " << VectInt.capacity() << std::endl;
	for (std::vector<int>::size_type i = 0, Capacity = 0; i != 100; ++i)
	{
		VectInt.push_back(i);
		if (Capacity != VectInt.capacity())
		{
			Capacity = VectInt.capacity();
			std::cout << "Capacity increased to " << Capacity;
			std::cout << " when vector increased from " << i << " to " << i + 1 << " size." << std::endl;
		}

	}

	return 0;
}