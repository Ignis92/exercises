// EXERCISE 7.7: Rewrite the transaction-processing
// program you wrote for the exercises in � 7.1.2 (p. 260)
// to use these new functions.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>


int main()
{
	SalesData Total;

	if (Read(std::cin, std::cout, Total))
	{
		SalesData Tmp;
		while (Read(std::cin, std::cout, Tmp))
		{
			if (Total.ISBN() == Tmp.ISBN())
			{
				Total.Combine(Tmp);
			}
			else
			{
				Total = Tmp;
			}
			Print(std::cout, Total);
		}
		return 0;
	}
	else
	{
		std::cout << "ISBNs differ!" << std::endl;
		return -1;
	}
}