// EXERCISE 6.3: Write and test your own version of fact.

#include "stdafx.h"
#include <iostream>

int Factorial(int N)
{
	int Result = 1;
	while (N != 0)
	{
		Result *= N--;
	}
	return Result;
}

int main()
{
	int Num;
	std::cout << "Input a number: ";
	std::cin >> Num;
	std::cout << "The factorial of " << Num << " is " << Factorial(Num) << std::endl;
	return 0;
}