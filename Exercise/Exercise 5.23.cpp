// EXERCISE 5.23: Write a program that reads two
// integers from the standard input and prints the result of
// dividing the first number by the second.

#include "stdafx.h"
#include <iostream>

int main()
{
	int Int1, Int2, Result;

	std::cout << "Input the first integer: ";
	std::cin >> Int1;
	std::cout << "Input the second integer: ";
	std::cin >> Int2;

	Result = Int1 / Int2;
	std::cout << "The result of dividing the first number by the second one is: " << Result << std::endl;

	return 0;
}