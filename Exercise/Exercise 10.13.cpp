// EXERCISE 10.13: The library defines an algorithm
// named partition that takes a predicate and partitions
// the container so that values for which the predicate is
// true appear in the first part and those for which the
// predicate is false appear in the second part. The
// algorithm returns an iterator just past the last element
// for which the predicate returned true. Write a function
// that takes a string and returns a bool indicating whether
// the string has five characters or more. Use that function
// to partition words. Print the elements that have five or
// more characters.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

bool IsSizeOk(const std::string &Str)
{
	return Str.size() < 5;
}

int main()
{
	std::vector<std::string> words{ "the","quick","red","fox","jumps","over","the","slow","red","turtle" };

	auto PartIter = std::partition(words.begin(), words.end(), IsSizeOk);
	std::cout << "The words big enough are:" << std::endl;
	for (; PartIter != words.end(); ++PartIter)
	{
		std::cout << *PartIter << std::endl;
	}

	return 0;
}