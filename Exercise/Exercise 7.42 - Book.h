// EXERCISE 7.42: For the class you wrote for exercise
// 7.40 in � 7.5.1 (p. 291), decide whether any of the
// constructors might use delegation. If so, write the
// delegating constructor(s) for your class. If not, look at
// the list of abstractions and choose one that you think
// would use a delegating constructor. Write the class
// definition for that abstraction.

#pragma once

#include <string>

class Book
{
public:
	Book(std::string T, unsigned N, double C, bool E) : Title(T), NumberOfPages(N), Cost(C), isEconomicVersion(E) {}
	Book() : Book("", 0, 0, false) {}
	Book(std::string T) : Book(T, 0, 0, false) {}
	Book(std::string T, unsigned N) : Book(T, N, 0, false) {}
	Book(std::string T, double C) : Book(T, 0, C, false) {}
	Book(std::string T, bool E) : Book(T, 0, 0, E) {}
	Book(std::string T, unsigned N, double C) : Book(T, N, C, false) {}
	Book(std::string T, unsigned N, bool E) : Book(T, N, 0, E) {}
	Book(std::string T, double C, bool E) : Book(T, 0, C, E) {}

private:
	std::string Title;
	unsigned NumberOfPages = 0;
	double Cost = 0;
	bool isEconomicVersion = false;
};