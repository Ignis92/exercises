// EXERCISE 9.31: The program on page 354 to remove even-
// valued elements and duplicate odd ones will not work on a
// list or forward_list. Why? Revise the program so that it
// works on these types as well.

#include "stdafx.h"
#include <iostream>
#include <list>
#include <forward_list>

int main()
{
	// List
	/*
	std::list<int> ListInt{ 0,1,2,3,4,5,6,7,8,9 };

	auto Iter = ListInt.cbegin();
	while (Iter != ListInt.cend())
	{
		if (*Iter % 2)
		{
			Iter = ListInt.insert(Iter, *Iter);
			++(++Iter);
		}
		else
		{
			Iter = ListInt.erase(Iter);
		}
	}
	*/

	// Forward list
	std::forward_list<int> ListInt{ 0,1,2,3,4,5,6,7,8,9 };

	auto CurrIter = ListInt.cbegin(), PrevIter=ListInt.cbefore_begin();
	while (CurrIter != ListInt.cend())
	{
		if (*CurrIter % 2)
		{
			PrevIter = ListInt.insert_after(CurrIter, *CurrIter);
			++(++CurrIter);
		}
		else
		{
			CurrIter = ListInt.erase_after(PrevIter);
		}
	}

	std::cout << "List after processing:" << std::endl;
	for (auto Elem:ListInt)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}