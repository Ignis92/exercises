// EXERCISE 5.25: Revise your program from the previous
// exercise to use a try block to catch the exception. The
// catch clause should print a message to the user and ask
// them to supply a new number and repeat the code
// inside the try.

#include "stdafx.h"
#include <iostream>
#include <stdexcept>

int main()
{
	int Int1, Int2 = 1, Result;
	while (true)
	{
		try
		{
			std::cout << "Input the first integer: ";
			std::cin >> Int1;
			std::cout << "Input the second integer: ";
			std::cin >> Int2;
			if (Int2 == 0)
			{
				throw std::runtime_error("Can't divide by zero");
			}
			break;
		}
		catch (std::runtime_error Err)
		{
			std::cout << Err.what() << std::endl;
			std::cout << "Please supply new numbers." << std::endl;
		}
	}
	Result = Int1 / Int2;
	std::cout << "The result of dividing the first number by the second one is: " << Result << std::endl;

	return 0;
}