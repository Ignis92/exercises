// EXERCISE 9.4: Write a function that takes a pair of
// iterators to a vector<int> and an int value. Look for that
// value in the range and return a bool indicating whether
// it was found.

#include "stdafx.h"
#include <iostream>
#include <vector>

bool LookforValue(std::vector<int>::const_iterator Start, std::vector<int>::const_iterator End, int Value)
{
	for (; Start != End && Value != *Start; ++Start)
	{

	}

	if (Start == End)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int main()
{
	std::vector<int> VectInt{ 1,3,64,7,2,10,5682,123,5,323 };

	auto Start = VectInt.begin();
	auto End = VectInt.end();

	if (LookforValue(Start, End, 10))
	{
		std::cout << "Found it!" << std::endl;
	}
	else
	{
		std::cout << "Value not found!" << std::endl;
	}

	return 0;
}