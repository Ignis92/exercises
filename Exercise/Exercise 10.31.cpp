// EXERCISE 10.31: Update the program from the previous
// exercise so that it prints only the unique elements. Your
// program should use unqiue_copy (§ 10.4.1, p. 403).

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

int main()
{
	std::istream_iterator<int> InputInt(std::cin), EndOfFile;
	std::vector<int> VectInt(InputInt,EndOfFile);
	
	sort(VectInt.begin(), VectInt.end(), [](int Val1, int Val2) { return Val1 < Val2; });

	std::ostream_iterator<int> OutputInt(std::cout, "\n");
	
	std::cout << "The sorted vector is:" << std::endl;
	std::unique_copy(VectInt.cbegin(), VectInt.cend(), OutputInt);

	return 0;
}