// EXERCISE 9.15: Write a program to determine whether
// two vector<int>s are equal.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> Vect1{ 1,2,3,4 }, Vect2{ 1,2,3,5 };

	if (Vect1 == Vect2)
	{
		std::cout << "Vectors are equal" << std::endl;
	}
	else
	{
		if (Vect1 > Vect2)
		{
			std::cout << "The first vector is greater" << std::endl;
		}
		else
		{
			std::cout << "The second vector is greater" << std::endl;
		}
	}

	return 0;
}