#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	std::string ISBN() const { return BookNumber; }
	SalesData &Combine(const SalesData &ToAdd)

	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

SalesData &SalesData::Combine(const SalesData &ToAdd)
{
	UnitSold += ToAdd.UnitSold;
	Revenue += ToAdd.Revenue;
	return *this;
}

SalesData Add(const SalesData &Data1, const SalesData &Data2)
{
	SalesData Sum = Data1;
	Sum.Combine(Data2);

	return Sum;
}

std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target)
{
	Output << "Input the ISBN: ";

	if (Input >> Target.BookNumber)
	{
		Output << "Input number of unit sold: ";

		if (Input >> Target.UnitSold)
		{
			Output << "Input the revenue: ";
			Input >> Target.Revenue;

			return Input;
		}
		else
		{
			return Input;
		}
	}
	else
	{
		return Input;
	}
}

std::ostream &Print(std::ostream &Output, const SalesData &Target)
{
	Output << "Book ISBN: " << Target.BookNumber << std::endl;
	Output << "Books sold: " << Target.UnitSold << std::endl;
	Output << "Total revenue: " << Target.Revenue << std::endl;

	return Output;
}