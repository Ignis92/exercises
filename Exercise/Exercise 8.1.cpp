// EXERCISE 8.1: Write a function that takes and returns
// an istream&. The function should read the stream until it
// hits end - of - file. The function should print what it reads
// to the standard output. Reset the stream so that it is
// valid before returning the stream.


#include "stdafx.h"
#include <iostream>
#include <string>

std::istream &Read(std::istream &Input)
{
	std::string Elem;
	while (Input >> Elem)
	{
		std::cout << Elem << std::endl;
	}


	std::cin.clear();
	return Input;
}

int main()
{
	Read(std::cin);

	return 0;
}