// EXERCISE 9.24: Write a program that fetches the first
// element in a vector using at, the subscript operator,
// front, and begin. Test your program on an empty vector.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> VectInt;

	std::cout << "Using at: " << VectInt.at(0) << std::endl;
	std::cout << "Using subscript: " << VectInt[0] << std::endl;
	std::cout << "Using front: " << VectInt.front() << std::endl;
	std::cout << "Using begin: " << *VectInt.begin() << std::endl;

	return 0;
}