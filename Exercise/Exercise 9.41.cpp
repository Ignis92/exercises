// EXERCISE 9.41: Write a program that initializes a string
// from a vector<char>.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<char> VectChar{ 'a','b','c' };
	std::string Str(VectChar.cbegin(), VectChar.cend());

	std::cout << "String is:" << std::endl;
	std::cout << Str << std::endl;

	return 0;
}