// EXERCISE 3.2: Write a program to read the standard
// input a line at a time. Modify your program to read a
// word at a time.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Str;
	std::cout << "Input a string:" << std::endl;
	while (std::cin >> Str) // getline(std::cin, Str) for the entire line
	{
		std::cout << Str << std::endl;
		std::cout << "Input a string or ctrl-z to terminate:" << std::endl;
	}

	return 0;
}