// EXERCISE 3.23: Write a program to create a vector with
// ten int elements.Using an iterator, assign each element
// a value that is twice its current value. Test your
// program by printing the vector.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> IntVect{ 1,5,3,8,10,4,7,9,2,6 };

	std::cout << "The starting vector is" << std::endl;
	for (auto it = IntVect.cbegin(); it != IntVect.cend(); ++it)
	{
		std::cout << *it << std::endl;
	}

	for (auto it = IntVect.begin(); it != IntVect.end(); ++it)
	{
		*it *= *it;
	}

	std::cout << "The multiplied vector is:" << std::endl;
	for (auto it = IntVect.cbegin(); it != IntVect.cend(); ++it)
	{
		std::cout << *it << std::endl;
	}
	return 0;
}