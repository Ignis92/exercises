// EXERCISE 7.13: Rewrite the program from page 255 to
// use the istream constructor.

#include "stdafx.h"
#include "SalesData.h"
#include <iostream>


int main()
{
	SalesData Total(std::cin, std::cout);
	if (std::cin)
	{
		while(std::cin)
		{
			SalesData Tmp(std::cin, std::cout);
			if (std::cin)
			{
				if (Total.ISBN() == Tmp.ISBN())
				{
					Total.Combine(Tmp);
				}
				else
				{
					Print(std::cout, Total);
					Total = Tmp;
				}
			}
		}
		Print(std::cout, Total);
	}
	else
	{
		std::cout << "No data?!" << std::endl;
	}
	return 0;
}