#include "stdafx.h"
#include "SalesData.h"

/*
SalesData::SalesData(std::istream & Input, std::ostream & Output)
{
	Output << "Input the book ISBN: ";
	if (Input >> BookNumber)
	{
		Output << "Input the number of copies sold: ";
		if (Input >> UnitSold)
		{
			Output << "Input the revenue: ";
			if (!(Input >> Revenue))
			{
				Output << "Incorrect revenue input." << std::endl;
			}
		}
		else
		{
			Output << "Incorrect number input." << std::endl;
		}
	}
	else
	{
		Output << "Incorrect ISBN input." << std::endl;
	}
}
*/

SalesData &SalesData::Combine(const SalesData &ToAdd)
{
	UnitSold += ToAdd.UnitSold;
	Revenue += ToAdd.Revenue;
	return *this;
}

SalesData Add(const SalesData &Data1, const SalesData &Data2)
{
	SalesData Sum = Data1;
	Sum.Combine(Data2);

	return Sum;
}

std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target)
{
	Output << "Input the ISBN: ";

	if (Input >> Target.BookNumber)
	{
		Output << "Input number of unit sold: ";

		if (Input >> Target.UnitSold)
		{
			Output << "Input the revenue: ";
			Input >> Target.Revenue;

			return Input;
		}
		else
		{
			return Input;
		}
	}
	else
	{
		return Input;
	}
}

std::ostream &Print(std::ostream &Output, const SalesData &Target)
{
	Output << "Book ISBN: " << Target.BookNumber << std::endl;
	Output << "Books sold: " << Target.UnitSold << std::endl;
	Output << "Total revenue: " << Target.Revenue << std::endl;

	return Output;
}