// EXERCISE 10.15: Write a lambda that captures an int from
// its enclosing function and takes an int parameter. The
// lambda should return the sum of the captured int and the
// int parameter.

#include "stdafx.h"
#include <iostream>

int main()
{
	int BaseVal = 5, Add = 3;
	auto f = [BaseVal](int Int) { return BaseVal + Int; };
	BaseVal = 9;
	std::cout << "The sum is: " << f(Add) << std::endl; // BaseVal in f is still 5!

	return 0;
}