// EXERCISE 6.27: Write a function that takes an
// initializer_list<int> and produces the sum of the
// elements in the list.

#include "stdafx.h"
#include <iostream>

int Sum(std::initializer_list<int> NumbersToSum)
{
	int Sum = 0;

	for (auto Number : NumbersToSum)
	{
		Sum += Number;
	}

	return Sum;
}

int main()
{
	int Int1 = 3, Int2 = 5;

	std::cout << "Total sum: " << Sum({ Int1,Int2 }) << std::endl;

	return 0;
}