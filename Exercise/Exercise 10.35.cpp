// EXERCISE 10.35: Now print the elements in reverse order
// using ordinary iterators.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> VectInt{ 0,1,2,3,4,5,6,7,8,9 };

	std::cout << "Printing the vector in reverse order:" << std::endl;
	for (auto Iter = --VectInt.cend(); Iter != VectInt.cbegin(); --Iter)
	{
		std::cout << *Iter << std::endl;
	}
	std::cout << *VectInt.cbegin() << std::endl;

	return 0;
}