// EXERCISE 5.17: Given two vectors of ints, write a program
// to determine whether one vector is a prefix of the other. For
// vectors of unequal length, compare the number of elements
// of the smaller vector. For example, given the vectors
// containing 0, 1, 1, and 2 and 0, 1, 1, 2, 3, 5, 8, respectively
// your program should return true.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	int Elem;
	std::vector<int> Vect1, Vect2;

	std::cout << "Input the first vector." << std::endl;
	std::cout << "Input an element or ctrl-z to terminate: ";
	while (std::cin >> Elem)
	{
		Vect1.push_back(Elem);
		std::cout << "Input the next element or ctrl-z to terminate: ";
	}
	std::cin.clear(); // TODO remove need to input enter after ctrl-z 
	std::cin.ignore();

	std::cout << std::endl;
	std::cout << "Input the second vector." << std::endl;
	std::cout << "Input an element or ctrl-z to terminate: ";
	while (std::cin >> Elem)
	{
		Vect2.push_back(Elem);
		std::cout << "Input the next element or ctrl-z to terminate: ";
	}
	std::cin.clear();
	std::cin.ignore();

	bool Test = true;
	for (decltype(Vect1.size()) i = 0, sz = (Vect1.size() < Vect2.size()) ? Vect1.size() : Vect2.size();
		i != sz && Test == true; ++i)
	{
		if (Vect1[i] != Vect2[i])
		{
			Test = false;
		}
	}

	if (Vect1.size() < Vect2.size())
	{
		if (Test == true)
		{
			std::cout << "First vector is a prefix of the second one." << std::endl;
		}
		else
		{
			std::cout << "First vector is smaller, but it's not a prefix of the second one." << std::endl;
		}
	}
	else
	{
		if (Vect1.size() == Vect2.size())
		{
			if (Test == true)
			{
				std::cout << "Vectors are the same." << std::endl;
			}
			else
			{
				std::cout << "Vectors have the same length but are not the same." << std::endl;
			}
		}
		else
		{
			if (Test == true)
			{
				std::cout << "Second vector is a prefix of the first one." << std::endl;
			}
			else
			{
				std::cout << "Second vector is smaller, but it's not a prefix of the first one." << std::endl;
			}
		}
	}

	return 0;
}