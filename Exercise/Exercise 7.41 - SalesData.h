#pragma once

#include <iostream>
#include <string>

struct SalesData
{
	// Friend
	friend SalesData Add(const SalesData &Data1, const SalesData &Data2);
	friend std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
	friend std::ostream &Print(std::ostream &Output, const SalesData &Target);

public:
	// Constructor
	SalesData(const std::string S, unsigned U, double R) : BookNumber(S), UnitSold(U), Revenue(R) { std::cout << "Complete constructor" << std::endl; }
	SalesData() : SalesData("", 0, 0) { std::cout << "Empty constructor" << std::endl; }
	SalesData(const std::string S) : SalesData(S, 0, 0) { std::cout << "String constructor" << std::endl; }
	SalesData(std::istream & Input, std::ostream & Output) : SalesData() { std::cout << "Read constructor" << std::endl; Read(std::cin, std::cout, *this); }


	std::string ISBN() const { return BookNumber; }

	SalesData &Combine(const SalesData &ToAdd);
	double AvgPrice() const { return Revenue / UnitSold; }

private:
	std::string BookNumber;
	unsigned UnitSold = 0;
	double Revenue = 0;
};

SalesData Add(const SalesData &Data1, const SalesData &Data2);
std::istream &Read(std::istream &Input, std::ostream &Output, SalesData &Target);
std::ostream &Print(std::ostream &Output, const SalesData &Target);