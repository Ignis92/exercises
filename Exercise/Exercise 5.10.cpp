// EXERCISE 5.10: There is one problem with our vowelcounting
// program as we�ve implemented it: It doesn�t count
// capital letters as vowels. Write a program that counts both
// lower - and uppercase letters as the appropriate vowel � that
// is, your program should count both 'a' and 'A' as part of
// aCnt, and so forth.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Line;

	std::cout << "Input a line: ";
	while (getline(std::cin, Line))
	{
		int vCnt = 0, oCnt = 0;
		for (const auto c : Line)
		{
			switch (toupper(c))
			{
			case 'A': case 'E': case 'I': case 'O': case 'U':
				++vCnt;
				break;
			default:
				++oCnt;
				break;
			}
		}

		std::cout << "The number of vowels in the inserted line is: " << vCnt << std::endl;
		std::cout << "Input a line or ctrl-z to terminate: ";
	}

	return 0;
}