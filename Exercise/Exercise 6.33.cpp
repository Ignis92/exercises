// EXERCISE 6.33: Write a recursive function to print the
// contents of a vector.

#include "stdafx.h"
#include <iostream>
#include <vector>

std::vector<int> RecursivePrint(std::vector<int> VectorToPrint)
{
	if (VectorToPrint.size())
	{
		std::cout << VectorToPrint[VectorToPrint.size() - 1] << std::endl;
		VectorToPrint.pop_back();
		return RecursivePrint(VectorToPrint);
	}
	else
	{
		return VectorToPrint;
	}
}

int main()
{
	std::vector<int> TestVector = { 1,2,3,4,5,6,7,8,9,10 };

	RecursivePrint(TestVector);

	return 0;
}