#include "stdafx.h"

// #include <iostream>

template<typename T>
void f(std::initializer_list<T> Vector)
{
	for (auto Elem : Vector)
	{
		std::cout << Elem;
	}
}

int main()
{
	f({ 1,5,7 });
}