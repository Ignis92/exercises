// EXERCISE 7.9: Add operations to read and print Person
// objects to the code you wrote for the exercises in �
// 7.1.2 (p. 260).

#pragma once

#include <string>

struct Person
{
	std::string GetName() const { return Name; }
	std::string GetAddress() const { return Address; }

	std::istream &Read(std::istream &Input, std::ostream &Output);
	std::ostream &Print(std::ostream &Output);

	std::string Name;
	std::string Address;
};