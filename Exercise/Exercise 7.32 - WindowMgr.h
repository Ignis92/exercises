// EXERCISE 7.32: Define your own versions of Screen and
// Window_mgr in which clear is a member of Window_mgr and
// a friend of Screen.

#pragma once

#include <vector>
#include "Screen.h"

class WindowMgr
{
public:
	using ScreenIndex = std::vector<Screen>::size_type;

	void Clear(ScreenIndex Target);

private:
	std::vector<Screen> Screens{ Screen(24,80,' ') };
};