// EXERCISE 3.17: Read a sequence of words from cin and
// store the values a vector. After you�ve read all the
// words, process the vector and change each word to
// uppercase. Print the transformed elements, eight words
// to a line.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

int main()
{
	std::vector<std::string> Vect;
	std::string Elem;

	std::cout << "Input a single word:" << std::endl;
	while (std::cin >> Elem)
	{
		Vect.push_back(Elem);
		std::cout << "Input a single word or ctrl-z to terminate:" << std::endl;
	}

	for (decltype(Vect.size()) i = 0; i < Vect.size(); ++i)
	{
		for (auto &c : Vect[i])
		{
			c = toupper(c);
		}
	}

	int Count = 0;
	for (decltype(Vect.size()) i = 0; i < Vect.size(); ++i)
	{
		std::cout << Vect[i] << " ";
		++Count;
		if (Count == 8)
		{
			std::cout << std::endl;
			Count = 0;
		}
	}
	std::cout << std::endl;

	return 0;
}