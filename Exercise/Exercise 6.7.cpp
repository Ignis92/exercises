// EXERCISE 6.7: Write a function that returns 0 when it is
// first called and then generates numbers in sequence
// each time it is called again.

#include "stdafx.h"
#include <iostream>

int DumbFunct(void)
{
	// it would be better to set Cnt = -1 performance-wise, so we can turn
	// the postfix increment into a prefix increment and avoid the copy overhead
	static int Cnt = 0;

	return Cnt++;
}

int main()
{
	for (int i = 0; i != 10; ++i)
	{
		std::cout << "Executing DumbFunct... " << DumbFunct() << std::endl;
	}

	return 0;
}