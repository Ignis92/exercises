// EXERCISE 3.44: Rewrite the programs from the previous
// exercises using a type alias for the type of the loop
// control variables.

#include "stdafx.h"
#include <iostream>

int main()
{
	int ia[3][4] = { { 1,2,3,4 },{ 5,6,7,8 },{ 9,10,11,12 } };

	std::cout << "The bidimensional array is:" << std::endl;
	using int_array = int[std::size(ia[0])];

	// Range for
	for (int_array &i : ia)
	{
		for (int j : i)
		{
			std::cout << j << " ";
		}
		std::cout << std::endl;
	}

	// Subscripts
	for (int i = 0; i < std::size(ia); ++i)
	{
		for (int j = 0; j < std::size(ia[0]); ++j)
		{
			std::cout << ia[i][j] << " ";
		}
		std::cout << std::endl;
	}

	// Pointers
	for (int_array *i = std::begin(ia); i != std::end(ia); ++i)
	{
		for (int *j = std::begin(*i); j != std::end(*i); ++j)
		{
			std::cout << *j << " ";
		}
		std::cout << std::endl;
	}

	return 0;
}