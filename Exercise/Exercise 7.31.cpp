// EXERCISE 7.31: Define a pair of classes X and Y, in
// which X has a pointer to Y, and Y has an object of type
// X.

#include "stdafx.h"
#include <iostream>

// Forward declaration
class Y;

class X
{
	Y *PointerToY;
};

class Y
{
	X ObjectX;
};

int main()
{
	X InstanceX;
	Y InstanceY;
	return 0;
}