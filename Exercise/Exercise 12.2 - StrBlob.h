// EXERCISE 12.2: Write your own version of the StrBlob class
// including the const versions of front and back.

#pragma once

#include <initializer_list>
#include <memory>
#include <string>
#include <vector>

class StrBlob
{
public:
	/// Constructor
	StrBlob() : Data(std::make_shared<std::vector<std::string>>()) {}
	StrBlob(std::initializer_list<std::string> InitList) : Data(std::make_shared<std::vector<std::string>>(InitList)) {}
	
	/// Get utility
	std::vector<std::string>::size_type Size() const { return Data->size(); }
	bool IsEmpty() const { return Data->size(); }

	/// Element addition or deletion
	void PushBack(const std::string &Str) { Data->push_back(Str); }
	void PushBack(const std::string &Str) const { Data->push_back(Str); }
	void PopBack() { Data->pop_back(); }
	void PopBack() const { Data->pop_back(); }

	/// Element access
	std::string & Front() { return Data->front(); }
	std::string & Front() const { return Data->front(); }
	std::string & Back() { return Data->back(); }
	std::string & Back() const { return Data->back(); }

private:
	std::shared_ptr<std::vector<std::string>> Data;
};

