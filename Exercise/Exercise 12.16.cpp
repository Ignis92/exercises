// EXERCISE 12.16: Compilers don’t always give easy-to-
// understand error messages if we attempt to copy or
// assign a unique_ptr. Write a program that contains these
// errors to see how your compiler diagnoses them.

#include "stdafx.h"
#include <iostream>
#include <memory>

int main()
{
	std::unique_ptr<int> UP1(new int(10)), UP2(new int(15));
	UP1 = UP2;

	return 0;
}