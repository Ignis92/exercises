// EXERCISE 3.41: Write a program to initialize a vector
// from an array of ints.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	int Arr[] = { 1,2,3,4,5 };
	std::vector<int> Vect(std::begin(Arr), std::end(Arr));

	std::cout << "The vector has the following elements:" << std::endl;
	for (auto i : Vect)
	{
		std::cout << i << std::endl;
	}

	return 0;
}