// EXERCISE 10.30: Use stream iterators, sort, and copy to
// read a sequence of integers from the standard input, sort
// them, and then write them back to the standard output.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

int main()
{
	std::istream_iterator<int> InputInt(std::cin), EndOfFile;
	std::vector<int> VectInt(InputInt,EndOfFile);
	
	sort(VectInt.begin(), VectInt.end(), [](int Val1, int Val2) { return Val1 < Val2; });

	std::ostream_iterator<int> OutputInt(std::cout, "\n");
	
	std::cout << "The sorted vector is:" << std::endl;
	copy(VectInt.cbegin(), VectInt.cend(), OutputInt);

	return 0;
}