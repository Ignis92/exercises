// EXERCISE 11.16: Using a map iterator write an expression
// that assigns a value to an element.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main()
{
	std::map<std::string, int> Map{ {"Hi",1},{"Hello",2},{"Good morning",3} };

	auto Iter = Map.find("Hello");
	Iter->second = 5;

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : Map)
	{
		std::cout << "String: " << Elem.first << "; Integer: " << Elem.second << "." << std::endl;
	}

	return 0;
}