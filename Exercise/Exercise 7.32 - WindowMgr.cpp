#include "stdafx.h"
#include "WindowMgr.h"

void WindowMgr::Clear(ScreenIndex Target)
{
	Screen &Tmp = Screens[Target];
	Tmp.Contents = std::string(Tmp.Height * Tmp.Width, ' ');
}
