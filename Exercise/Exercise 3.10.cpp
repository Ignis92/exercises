// EXERCISE 3.10: Write a program that reads a string of
// characters including punctuation and writes what was read
// but with the punctuation removed.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::cout << "Input a string:" << std::endl;
	std::string StartStr, FinalStr;
	getline(std::cin, StartStr);
	std::cout << "Starting string:" << std::endl;
	std::cout << StartStr << std::endl;
	for (auto c : StartStr)
	{
		if (!ispunct(c))
		{
			FinalStr += c;
		}
	}

	std::cout << "Final string:" << std::endl;
	std::cout << FinalStr << std::endl;
	return 0;
}