// EXERCISE 5.9: Write a program using a series of if
// statements to count the number of vowels in text read from
// cin.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string Line;

	std::cout << "Input a line: ";
	while (getline(std::cin, Line))
	{
		int vCnt = 0;
		for (const auto c : Line)
		{
			if (toupper(c) == 'A')
			{
				++vCnt;
			}
			else
			{
				if (toupper(c) == 'E')
				{
					++vCnt;
				}
				else
				{
					if (toupper(c) == 'I')
					{
						++vCnt;
					}
					else
					{
						if (toupper(c) == 'O')
						{
							++vCnt;
						}
						else
						{
							if (toupper(c) == 'U')
							{
								++vCnt;
							}
						}
					}
				}
			}
		}

		std::cout << "The number of vowels in the inserted line is: " << vCnt << std::endl;
		std::cout << "Input a line or ctrl-z to terminate: ";
	}

	return 0;
}