// EXERCISE 6.4: Write a function that interacts with the user,
// asking for a number and generating the factorial of that
// number. Call this function from main.

#include "stdafx.h"
#include <iostream>

void Factorial(void)
{
	int Num, Result = 1, Cnt;

	std::cout << "Input a number: ";
	std::cin >> Num;

	Cnt = Num + 1;
	while (Cnt != 1)
	{
		Result *= --Cnt; // avoid copy with a postfix decrement
	}

	std::cout << "The factorial of " << Num << " is " << Result << std::endl;
	return;
}

int main()
{
	std::cout << "Calling factorial function..." << std::endl;
	Factorial();
	std::cout << "Function call terminated." << std::endl;
	return 0;
}