// EXERCISE 10.20: The library defines an algorithm named
// count_if. Like find_if, this function takes a pair of iterators
// denoting an input range and a predicate that it applies to
// each element in the given range. count_if returns a count of
// how often the predicate is true. Use count_if to rewrite the
// portion of our program that counted how many words are
// greater than length 6.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void ElimDups(std::vector<std::string> &Target)
{
	sort(Target.begin(), Target.end());
	auto Tail = std::unique(Target.begin(), Target.end());
	Target.erase(Tail, Target.end());
}

void Biggies(std::vector<std::string> &Target, std::string::size_type Size)
{
	ElimDups(Target);

	std::stable_sort(Target.begin(), Target.end(),
		[](const std::string &Str1, const std::string &Str2) { return Str1.size() < Str2.size(); });

	std::cout << "The " << std::count_if(Target.cbegin(), Target.cend(),
		[Size](const std::string &Str) {return Str.size() > Size; });
	std::cout << " word(s) big enough are (is): " << std::endl;
	for (auto Iter = std::stable_partition(Target.begin(), Target.end(),
		[Size](const std::string &Str) { return Str.size() <= Size; });
		Iter != Target.cend(); ++Iter)
	{
		std::cout << *Iter << std::endl;
	}

}

int main()
{
	std::vector<std::string> words{ "the","quick","red","fox","jumps","over","the","slow","red","turtle" };
	std::string::size_type Size = 3;

	Biggies(words, Size);

	return 0;
}