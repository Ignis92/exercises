// EXERCISE 3.5: Write a program to read strings from the
// standard input, concatenating what is read into one
// large string. Print the concatenated string. Next,
// change the program to separate adjacent input strings
// by a space.

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string StrCurr, StrTot;
	std::cout << "Input a string:" << std::endl;
	for (; getline(std::cin, StrCurr);)
	{
		std::cout << "Input a string or ctrl-z to terminate:" << std::endl;
		StrTot += " " + StrCurr; // delete the "space" string
	}
	std::cout << "The concatenated string is: " << StrTot << std::endl;
	return 0;
}