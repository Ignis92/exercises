// EXERCISE 11.31: Write a program that defines a
// multimap of authors and their works. Use find to find an
// element in the multimap and erase that element. Be sure
// your program works correctly if the element you look
// for is not in the map.

#include "stdafx.h"
#include <iostream>
#include <map>
#include <string>

int main()
{
	std::multimap<std::string, std::string> AuthorToWorkMap;
	std::string Author, Work;

	std::cout << "Adding a work and its author" << std::endl;
	std::cout << "Insert the name of the work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	while (getline(std::cin, Work) && getline(std::cin, Author))
	{
		AuthorToWorkMap.insert({ Author, Work });
		std::cout << "Insert the name of a new work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	}

	std::cin.clear();
	std::cout << "Deleting a work and its author" << std::endl;
	std::cout << "Insert the name of the work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	while (getline(std::cin, Work) && getline(std::cin, Author))
	{
		auto Iter = AuthorToWorkMap.find(Author);
		if (Iter == AuthorToWorkMap.end())
		{
			std::cout << "Author not found!" << std::endl;
		}
		else
		{
			while (Iter->first == Author && Iter->second != Work)
			{
				++Iter;
			}
			if (Iter->first != Author)
			{
				std::cout << "Work not found!" << std::endl;
			}
			else
			{
				AuthorToWorkMap.erase(Iter);
			}
		}
		std::cout << "Insert the name of a new work and then the name of the author or ctrl-z if you want to skip" << std::endl;
	}

	std::cout << "The map is:" << std::endl;
	for (const auto &Elem : AuthorToWorkMap)
	{
		std::cout << "Work " << Elem.second << " from author " << Elem.first << std::endl;
	}

	return 0;
}