// EXERCISE 10.17: Rewrite exercise 10.12 from § 10.3.1 (p.
// 387) to use a lambda in the call to sort instead of the
// compareIsbn function.

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include "SalesData.h"

int main()
{
	std::vector<SalesData> VectSalesData{ SalesData("IT10",5,28),SalesData("SPA15",3,15),SalesData("D1",4,25) };

	sort(VectSalesData.begin(), VectSalesData.end(),
		[](const SalesData &Obj1, const SalesData &Obj2) {return Obj1.ISBN().size() < Obj2.ISBN().size(); });

	std::cout << "The vector is:" << std::endl;
	for (const auto Elem : VectSalesData)
	{
		Print(std::cout, Elem);
	}

	return 0;
}