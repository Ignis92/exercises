// EXERCISE 6.11: Write and test your own version of
// reset that takes a reference.

#include "stdafx.h"
#include <iostream>

void Reset(int &Target)
{
	std::cout << "Resetting variable..." << std::endl;
	Target = 0;
	std::cout << "Reset succesful." << std::endl;
	return;
}

int main()
{
	int TestInt = 10;

	std::cout << "Before reset: " << TestInt << "." << std::endl;

	Reset(TestInt);

	std::cout << "After reset: " << TestInt << "." << std::endl;

	return 0;
}