// EXERCISE 10.27: In addition to unique(§ 10.2.3, p. 384),
// the library defines function named unique_copy that takes a
// third iterator denoting a destination into which to copy the
// unique elements. Write a program that uses unique_copy to
// copy the unique elements from a vector into an initially
// empty list.

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <vector>

int main()
{
	std::vector<int> VectOrig{ 0,1,1,2,3,3,3,4,4,5,6,6,7,7,7,7,8,8,8,9,9,9 }, VectCopy;

	std::unique_copy(VectOrig.cbegin(), VectOrig.cend(), back_inserter(VectCopy));

	std::cout << "The copied vector is:" << std::endl;
	for (const auto Elem : VectCopy)
	{
		std::cout << Elem << std::endl;
	}

	return 0;
}