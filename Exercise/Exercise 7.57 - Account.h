// EXERCISE 7.57: Write your own version of the Account
// class.

#pragma once

class Account
{
public:
	void Calculate() { Amount += Amount * InterestRate; }

	static double GetInterestRate() { return InterestRate; }

	static void SetInterestRate(double R) { InterestRate = R; }

private:
	double Amount;
	static double InterestRate;
};