// EXERCISE 7.53: Define your own version of Debug.

#pragma once

class Debug
{
public:
	// Constructors
	constexpr Debug(bool A = true) : HardwareErrors(A), IOErrors(A), OtherErrors(A) {}
	constexpr Debug(bool H, bool IO, bool O) : HardwareErrors(H), IOErrors(IO), OtherErrors(O) {}

	constexpr bool AnyErrors() { return HardwareErrors || IOErrors || OtherErrors; }

	void SetHardware(bool H) { HardwareErrors = H; }
	void SetIO(bool IO) { IOErrors = IO; }
	void SetOther(bool O) { OtherErrors = O; }

private:
	bool HardwareErrors;
	bool IOErrors;
	bool OtherErrors;
};