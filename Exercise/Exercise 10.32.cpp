// EXERCISE 10.32: Rewrite the bookstore problem from § 1.6
// (p. 24) using a vector to hold the transactions and various
// algorithms to do the processing. Use sort with your
// compareIsbn function from § 10.3.1 (p. 387) to arrange the
// transactions in order, and then use find and accumulate to do
// the sum.

// IMPOSSIBLE WITH THE GIVEN CLASS! IT DOESN'T HAVE THE NEEDED OVERLOADED OPERATORS

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include "SalesData.h"

bool compareISBN(const SalesData &Obj1, const SalesData &Obj2)
{
	return Obj1.ISBN().size() < Obj2.ISBN().size();
}

int main()
{
	SalesData CurrTrans; // variable to hold data for the next transaction
	std::vector<SalesData> Transactions;

	// read the first transaction and ensure that there are data to process
	while (Read(std::cin, std::cout, CurrTrans))
	{
		Transactions.push_back(CurrTrans);
	}

	if (Transactions.empty()) // no input! warn the user
	{
		std::cerr << "No data?!" << std::endl;
		return -1; // indicate failure
	}
	else
	{
		std::sort(Transactions.begin(), Transactions.end(), compareISBN);
		for (auto Iter=)
		std::find_if(Transactions.cbegin(), Transactions.cend(), []);
	}
	return 0;
}