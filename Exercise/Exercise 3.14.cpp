// EXERCISE 3.14: Write a program to read a sequence of ints
// from cin and store those values in a vector.

#include "stdafx.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> Vector;
	int NewElement;

	std::cout << "Input a number:" << std::endl;
	while (std::cin >> NewElement)
	{
		Vector.push_back(NewElement);
		std::cout << "Input a number or ctrl-z to end:" << std::endl;
	}

	std::cout << "Final vector:" << std::endl;

	for (auto i : Vector)
	{
		std::cout << i << std::endl;
	}

	return 0;
}