// EXERCISE 5.24: Revise your program to throw an
// exception if the second number is zero. Test your
// program with a zero input to see what happens on your
// system if you don�t catch an exception.

#include "stdafx.h"
#include <iostream>
#include <stdexcept>

int main()
{

	int Int1, Int2, Result;

	std::cout << "Input the first integer: ";
	std::cin >> Int1;
	std::cout << "Input the second integer: ";
	std::cin >> Int2;
	if (Int2 == 0)
	{
		throw std::runtime_error("Can't divide by zero");
	}

	Result = Int1 / Int2;
	std::cout << "The result of dividing the first number by the second one is: " << Result << std::endl;

	return 0;
}